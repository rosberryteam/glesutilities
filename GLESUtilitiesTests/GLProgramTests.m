//
//  GLProgramTests.m
//  GLESUtilitiesTests
//
//  Created by Andrey Konoplyankin on 9/21/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <GLESUtilities/GLESUtilities.h>
@import Expecta;

static NSString* const vertex = SHADER_STRING
(
 attribute vec4 position;
 attribute vec2 coordinate;
 
 varying vec2 c;
 
 void main()
 {
     gl_Position = position;
     c = coordinate;
 }
 );

static NSString* const fragment = SHADER_STRING
(
 uniform highp float r;
 uniform highp float g;
 uniform highp float b;
 
 varying highp vec2 c;
 
 void main()
 {
     gl_FragColor = vec4(r * c.x, g * c.x, b * c.x, c.y);
 }
 );

@interface GLProgramTests : XCTestCase

@property GLContext* context;

@end

@implementation GLProgramTests

- (void)setUp
{
    [super setUp];
    
    self.context = [GLContext context];
    [self.context set];
}

- (void)testGLProgramPassNilParameters
{
    expect(^{ __unused id p = [[GLProgram alloc] initWithVertexShaderString:nil fragmentShaderString:nil context:nil]; }).to.raiseAny();
}

- (void)testGLProgramFail
{
    GLProgram* program = [[GLProgram alloc] initWithVertexShaderString:@"dummy" fragmentShaderString:@"dummy" context:self.context];
    expect([program link]).to.beFalsy();
    expect(program.isInitialized).to.beFalsy();
}

- (void)testGLProgramSucceess
{
    GLProgram* program = [[GLProgram alloc] initWithVertexShaderString:vertex fragmentShaderString:fragment context:self.context];
    [program addAttribute:@"position"];
    [program addAttribute:@"coordinate"];
    expect([program link]).to.beTruthy();
    expect(program.isInitialized).to.beTruthy();
    
    expect([program attributeIndex:@"position"]).to.equal(0);
    expect([program attributeIndex:@"coordinate"]).to.equal(1);
    
    expect([program uniformIndex:@"r"]).to.equal(0);
    expect([program uniformIndex:@"g"]).to.beInTheRangeOf(1, 2);
    expect([program uniformIndex:@"b"]).to.beInTheRangeOf(1, 2);
}

- (void)testGLProgramDeallocWhenLockContext
{
    [self.context lock];
    GLProgram* program = [[GLProgram alloc] initWithVertexShaderString:vertex fragmentShaderString:fragment context:self.context];
    [program cleanup];
    program = nil;
    [self.context unlock];
}

- (void)testGLCameraOutputProgram
{
    GLProgram* program = [GLCameraOutputProgram programWithSupportsFullYUVRange:YES context:self.context];
    expect(program.isInitialized).to.beTruthy();
}

- (void)testGLVideoOutputColorMatrix601Program
{
    GLProgram* program = [GLVideoOutputColorMatrix601Program program:self.context];
    expect(program.isInitialized).to.beTruthy();
}

- (void)testGLVideoOutputColorMatrix709Program
{
    GLProgram* program = [GLVideoOutputColorMatrix709Program program:self.context];
    expect(program.isInitialized).to.beTruthy();
}

@end
