//
//  GLProgramCacheTests.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 11/25/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <GLESUtilities/GLESUtilities.h>
@import Expecta;

@interface GLProgramCacheTests : XCTestCase

@property GLContext* context;

@end

@implementation GLProgramCacheTests

- (void)setUp
{
    [super setUp];
    
    self.context = [GLContext context];
    [self.context set];
}

- (void)testGLProgramCache
{
    GLProgram* program1 = [GLProgramCache programWithClass:[GLVideoOutputColorMatrix601Program class] context:self.context];
    GLProgram* program2 = [GLProgramCache programWithClass:[GLVideoOutputColorMatrix601Program class] context:self.context];
    
    expect(program1).to.beIdenticalTo(program2);
}

- (void)testManualCache
{
    GLProgram* program1 = [GLCameraOutputProgram programWithSupportsFullYUVRange:YES context:self.context];
    [GLProgramCache cacheProgram:program1];
    
    GLProgram* program2 = [GLProgramCache programWithClass:[GLCameraOutputProgram class] context:self.context];
    
    expect(program1).to.beIdenticalTo(program2);
}

@end
