//
//  GLMathTests.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 11/25/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <GLESUtilities/GLESUtilities.h>
#import <GLKit/GLKit.h>

#define expect(m1, m2) \
{ \
    NSMutableString* ma1 = [NSMutableString string]; \
    NSMutableString* ma2 = [NSMutableString string]; \
     \
    for (int i = 0; i < 16; i++) \
    { \
        [ma1 appendFormat:@"%@,", @(m1.pointer()[i])]; \
        [ma2 appendFormat:@"%@,", @(m2.m[i])]; \
    } \
     \
    XCTAssertEqualObjects(ma1, ma2); \
}


@interface GLMathTests : XCTestCase

@end

@implementation GLMathTests

- (void)testMatrixTranslation
{
    GLMatrix4x4 m1 = GLMatrix4x4::Translate(20, 10, 30);
    GLKMatrix4 m2 = GLKMatrix4MakeTranslation(20, 10, 30);
    
    expect(m1, m2);
}

- (void)testMatrixScale
{
    GLMatrix4x4 m1 = GLMatrix4x4::Scale(10, 20, 30);
    GLKMatrix4 m2 = GLKMatrix4MakeScale(10, 20, 30);
    
    expect(m1, m2);
}

- (void)testMatrixRotate
{
    GLMatrix4x4 m1 = GLMatrix4x4::RotationZ(M_PI);
    GLKMatrix4 m2 = GLKMatrix4MakeRotation(M_PI, 0, 0, 1);
    
    expect(m1, m2);
}

- (void)testMatrixOrtho
{
    GLMatrix4x4 m1 = GLMatrix4x4::Ortho(0, 128, 128, 0, -1, 1);
    GLKMatrix4 m2 = GLKMatrix4MakeOrtho(0, 128, 128, 0, -1, 1);
    
    expect(m1, m2);
}

- (void)testMatrixMultiple
{
    GLMatrix4x4 m1 = GLMatrix4x4::Translate(10, 20) * GLMatrix4x4::Scale(5);
    GLKMatrix4 m2 = GLKMatrix4Multiply(GLKMatrix4MakeTranslation(10, 20, 0), GLKMatrix4MakeScale(5, 5, 5));
    
    expect(m1, m2);
}

- (void)testVector
{
    GLMatrix4x4 m1 = GLMatrix4x4::LookAt(10, 20, 30, 1, 2, 3, 15, 30, 45);
    GLKMatrix4 m2 = GLKMatrix4MakeLookAt(10, 20, 30, 1, 2, 3, 15, 30, 45);
    
    expect(m1, m2);
}

@end
