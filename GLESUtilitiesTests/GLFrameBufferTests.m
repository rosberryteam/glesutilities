//
//  GLFrameBufferTests.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 11/25/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "GLImageUtilities.h"

#import <GLESUtilities/GLESUtilities.h>
@import Expecta;

@interface GLFrameBufferTests : XCTestCase

@property GLContext* context;

@end

@implementation GLFrameBufferTests

- (void)setUp
{
    [super setUp];
    
    self.context = [GLContext sharedContext];
    [self.context set];
}

- (void)testGLProgramPassNilParameters
{
    expect(^{ [GLFrameBuffer frameBufferWithFormat:GLFrameBufferFormatRGB565 size:CGSizeZero context:nil]; }).to.raiseAny();
    expect(^{ [GLFrameBuffer frameBufferWithGLTexture:nil context:nil]; }).to.raiseAny();
}

- (void)testFramebuffer
{
    CGSize size = CGSizeMake(128.0f, 128.0f);
    GLFrameBuffer* fbo = [GLFrameBuffer frameBufferWithFormat:GLFrameBufferFormatRGB565 size:size context:self.context];
    
    expect(fbo.handle).notTo.equal(0);
    expect(fbo.texHandle).notTo.equal(0);
    expect(fbo.size).to.equal(size);
}

- (void)testFramebufferWithTexture
{
    UIImage* image = GLImageUtilitiesColoredImage(CGSizeMake(128.0f, 128.0f), [UIColor redColor]);
    GLTexture* texture = [GLTexture textureWithImage:image andTextureFormat:GLTextureFormatRGB565 context:self.context];
    GLFrameBuffer* fbo = [GLFrameBuffer frameBufferWithGLTexture:texture context:self.context];
    
    expect(fbo.handle).notTo.equal(0);
    expect(fbo.texHandle).notTo.equal(0);
    expect(fbo.size).to.equal(texture.size);
}

- (void)testSnapshot
{
    CGSize size = CGSizeMake(128.0f, 128.0f);
    GLFrameBuffer* fbo = [GLFrameBuffer frameBufferWithFormat:GLFrameBufferFormatRGB565 size:size context:self.context];
    
    UIImage* snapshot = [fbo snapshot];
    
    expect(snapshot.size).to.equal(fbo.size);
}

- (void)testEqualSnapshot
{
    UIImage* image1 = GLImageUtilitiesImage();
    GLTexture* texture = [GLTexture textureWithImage:image1 andTextureFormat:GLTextureFormatRGB565 context:self.context];
    GLFrameBuffer* fbo = [GLFrameBuffer frameBufferWithGLTexture:texture context:self.context];
    
    UIImage* image2 = [fbo snapshot];
    
    CGFloat f = GLImageUtilitiesCompareSimilarityOfImages(image1, image2, YES, 10);
    expect(f).to.beGreaterThan(0.9f);
}

- (void)testCleanup
{
    CGSize size = CGSizeMake(128.0f, 128.0f);
    GLFrameBuffer* fbo = [GLFrameBuffer frameBufferWithFormat:GLFrameBufferFormatRGB565 size:size context:self.context];
    
    expect(fbo.handle).notTo.equal(0);
    expect(fbo.texHandle).notTo.equal(0);
    
    [fbo cleanup];
    
    expect(fbo.handle).to.equal(0);
    expect(fbo.texHandle).to.equal(0);
}

- (void)testCleanupWithTexture
{
    UIImage* image1 = GLImageUtilitiesImage();
    GLTexture* texture = [GLTexture textureWithImage:image1 andTextureFormat:GLTextureFormatRGB565 context:self.context];
    GLFrameBuffer* fbo = [GLFrameBuffer frameBufferWithGLTexture:texture context:self.context];
    
    expect(fbo.handle).notTo.equal(0);
    expect(fbo.texHandle).notTo.equal(0);
    expect(texture.texHandle).notTo.equal(0);
    
    [fbo cleanup];
    
    expect(fbo.handle).to.equal(0);
    expect(fbo.texHandle).notTo.equal(0);
    expect(texture.texHandle).notTo.equal(0);
    
    [texture cleanup];
    
    expect(fbo.handle).to.equal(0);
    expect(texture.texHandle).to.equal(0);
}

@end
