//
//  GLImageUtilities.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 11/25/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#import "GLImageUtilities.h"
#import "GLStillCamera+SimulatorSupport_RawImageData.h"

UIImage* GLImageUtilitiesImage()
{
    NSData* data = [NSData dataWithBytes:GLStillCamera_SimulatorSupport_RawImageData length:GLStillCamera_SimulatorSupport_RawImageData_Lenght];
    return [UIImage imageWithData:data];
}

UIImage* GLImageUtilitiesColoredImage(CGSize size, UIColor* color)
{
    UIGraphicsBeginImageContext(size);
    [color set];
    UIRectFill(CGRectMake(0.0f, 0.0f, size.width, size.height));
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

CGFloat GLImageUtilitiesCompareSimilarityOfImages(UIImage* image1, UIImage* image2, BOOL checkSize, NSInteger maxDiff)
{
    if (!CGSizeEqualToSize(image1.size, image2.size) && checkSize)
    {
        return 0.0f;
    }
    
    CGSize size = image1.size;
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault | kCGImageAlphaNoneSkipLast;
    size_t bitsPerComponent = 8;
    size_t bytesPerPixel = 4 * size.width;
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(NULL, size.width, size.height, bitsPerComponent, bytesPerPixel, colorSpace, bitmapInfo);
    
    CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, image1.size.width, image1.size.height), image1.CGImage);
    CGContextSetBlendMode(context, kCGBlendModeDifference);
    CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, image2.size.width, image2.size.height), image2.CGImage);
    
    SInt32* data = CGBitmapContextGetData(context);
    size_t counter = 0;
    
    for (NSInteger i = 0; i < size.width * size.height; i++)
    {
        char* p = (char *)&data[i];
        if (p[0] < maxDiff && p[1] < maxDiff && p[2] < maxDiff) counter++;
    }
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    return counter / (size.width * size.height);
}
