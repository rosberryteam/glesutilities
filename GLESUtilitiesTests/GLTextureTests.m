//
//  GLTextureTests.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 11/25/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "GLImageUtilities.h"

#import <GLESUtilities/GLESUtilities.h>
@import Expecta;

@interface GLTextureTests : XCTestCase

@property GLContext* context;

@end

@implementation GLTextureTests

- (void)setUp
{
    [super setUp];
    
    self.context = [GLContext sharedContext];
    [self.context set];
}

- (void)testNilContext
{
    expect(^{ [GLTexture textureWithTextureFormat:GLTextureFormatRGB565 context:nil]; }).to.raiseAny();
}

- (void)testLoadNilImage
{
    NSError* error = nil;
    GLTexture* texture = [GLTexture textureWithTextureFormat:GLTextureFormatRGB565 context:self.context];
    [texture loadTextureWithImage:nil size:CGSizeMake(4.0f, 4.0f) error:&error];
    
    expect(error.code).to.equal(400);
}

- (void)testLoadImage
{
    UIImage* image = GLImageUtilitiesColoredImage(CGSizeMake(128.0f, 128.0f), [UIColor redColor]);
    GLTexture* texture = [GLTexture textureWithImage:image andTextureFormat:GLTextureFormatRGB565 context:self.context];
    
    expect(texture.isLoaded).to.beTruthy();
    expect(texture.size).to.equal(image.size);
}

- (void)testLoadData
{
    UIImage* image = GLImageUtilitiesColoredImage(CGSizeMake(128.0f, 128.0f), [UIColor redColor]);
    NSData* data = UIImageJPEGRepresentation(image, 1.0f);
    
    GLTexture* texture = [GLTexture textureWithTextureFormat:GLTextureFormatRGBA8888 context:self.context];
    [texture loadTextureWithData:data size:image.size error:nil];
    
    expect(texture.isLoaded).to.beTruthy();
    expect(texture.size).to.equal(image.size);
}

- (void)testEmpty
{
    CGSize size = CGSizeMake(128.0f, 128.0f);
    GLTexture* texture = [GLTexture emptyTextureWithSize:size andTextureFormat:GLTextureFormatRGBA4444 context:self.context];
    
    expect(texture.isLoaded).to.beTruthy();
    expect(texture.size).to.equal(size);
}

- (void)testCleanup
{
    GLTexture* texture = [GLTexture textureWithImage:GLImageUtilitiesImage() andTextureFormat:GLTextureFormatRGB565 context:self.context];
    expect(texture.texHandle).notTo.equal(0);
    
    [texture cleanup];
    expect(texture.texHandle).to.equal(0);
}

@end
