//
//  GLImageUtilities.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 11/25/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

UIImage* GLImageUtilitiesImage();

UIImage* GLImageUtilitiesColoredImage(CGSize size, UIColor* color);

CGFloat GLImageUtilitiesCompareSimilarityOfImages(UIImage* image1, UIImage* image2, BOOL checkSize, NSInteger maxDiff);
