//
//  GLContextTests.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 11/25/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <GLESUtilities/GLESUtilities.h>

@import Expecta;

@interface GLContextTests : XCTestCase

@end

@implementation GLContextTests

- (void)testExtensions
{
    expect([GLContext extensions].count).to.beGreaterThan(1);
    expect([GLContext supportExtension:[GLContext extensions].lastObject]).to.beTruthy();
}

- (void)testMaxTextureSize
{
    [[GLContext sharedContext] set];
    
    GLint maxTextureSize = 0;
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize);
    
    expect([GLContext maximumTextureSize]).to.equal(maxTextureSize);
}

- (void)testMaxTextureSizeFit
{
    GLint maximumTextureSize = [GLContext maximumTextureSize];
    CGSize size = [GLContext sizeThatFitInMaxSupportSize:CGSizeMake(maximumTextureSize * 2.0f, maximumTextureSize)];
    
    expect(size).to.equal(CGSizeMake(maximumTextureSize, maximumTextureSize / 2.0f));
}

- (void)testNonSharedContexts
{
    GLContext* context1 = [GLContext context];
    GLContext* context2 = [GLContext context];
    
    [context1 set];
    
    GLTexture* texture = [GLTexture emptyTextureWithSize:CGSizeMake(4.0f, 4.0f) andTextureFormat:GLTextureFormatRGB565 context:context1];
    [texture bindTextureToGLProgram:nil uniform:0 textureUnit:GL_TEXTURE0];
    
    expect(glIsTexture(texture.texHandle)).to.equal(YES);
    
    [context2 set];
    
    expect(glIsTexture(texture.texHandle)).to.equal(NO);
}

- (void)testSharedContexts
{
    GLContext* context1 = [GLContext context];
    GLContext* context2 = [GLContext contextWithSharedContext:context1];
    
    [context1 set];
    
    GLTexture* texture = [GLTexture emptyTextureWithSize:CGSizeMake(4.0f, 4.0f) andTextureFormat:GLTextureFormatRGB565 context:context1];
    [texture bindTextureToGLProgram:nil uniform:0 textureUnit:GL_TEXTURE0];
    
    expect(glIsTexture(texture.texHandle)).to.equal(YES);
    
    [context2 set];
    
    expect(glIsTexture(texture.texHandle)).to.equal(YES);
}

- (void)testCrashWhenLockIsSameThread
{
    GLContext* context = [GLContext context];
    [context lock];
    
    expect(^{ [context lock]; [context unlock]; }).to.raiseAny();
}

- (void)testNonCrashWhenLockInSeparateThread
{
    BOOL __block done = NO;
    GLContext* context = [GLContext context];
    [context lock];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        expect(^{ [context lock]; [context unlock]; }).notTo.raiseAny();
        done = YES;
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [context unlock];
    });
    
    expect(done).will.beTruthy();
}

@end
