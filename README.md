[![Carthage compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)

**GLESUtilities**

* Supported platform: iOS

* Supported iOS version: 8.1+

* GLES Version: 2.0

*GLESUtilities* - few common tools for easy work with OpenGL ES. Supported create textures and framebuffers in various formats, vertex arrays and vertex buffer objects, GL programs, states and programs cache.

**Installing:**

*GLESUtilities* ships as dynamic framework, after builds you need add GLESUtilities.framework to embeded binaries of your target.

*GLESUtilities* also can installing over *Carthage*, for this just add this line on your Cartfile (maybe need change to your bitbucket name)

git "https://rock88a@bitbucket.org/rosberryteam/glesutilities.git" "master"