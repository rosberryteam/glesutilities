//
//  GLProgramCache.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 6/9/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLProgramCache.h"
#import "GLProgram.h"

@interface GLProgramCache ()

@property (nonatomic) NSMutableDictionary* contexts;

@end

@implementation GLProgramCache

+ (instancetype)sharedInstance
{
    static GLProgramCache* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

+ (id)programWithClass:(Class)programClass context:(GLContext *)context
{
    return [[self sharedInstance] programWithClass:programClass context:context];
}

+ (void)cacheProgram:(GLProgram *)program
{
    [[self sharedInstance] cacheProgram:program];
}

+ (void)contextDidDestroy:(GLContext *)context
{
    [[self sharedInstance] contextDidDestroy:context];
}

#pragma mark -

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.contexts = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)cacheProgram:(GLProgram *)program
{
    NSParameterAssert(program.context);
    
    NSValue* key = [NSValue valueWithNonretainedObject:program.context];
    NSMutableDictionary* programs = self.contexts[key];
    
    if (programs == nil)
    {
        programs = [NSMutableDictionary dictionary];
        self.contexts[key] = programs;
    }
    
    programs[NSStringFromClass(program.class)] = program;
}

- (id)programWithClass:(Class)programClass context:(GLContext *)context
{
    NSValue* key = [NSValue valueWithNonretainedObject:context];
    NSMutableDictionary* programs = self.contexts[key];
    
    if (programs == nil)
    {
        programs = [NSMutableDictionary dictionary];
        self.contexts[key] = programs;
    }
    
    if (programs[NSStringFromClass(programClass)])
    {
        return programs[NSStringFromClass(programClass)];
    }
    else
    {
        GLProgram* program = nil;
        if ([programClass respondsToSelector:@selector(program:)])
        {
            program = [programClass program:context];
        }
        else
        {
            [NSException raise:NSInternalInconsistencyException format:@"For use %s needs implement +program: in GLProgram!", __func__];
        }
        
        programs[NSStringFromClass(programClass)] = program;
        return program;
    }
}

- (void)contextDidDestroy:(GLContext *)context
{
    NSValue* key = [NSValue valueWithNonretainedObject:context];
    [self.contexts removeObjectForKey:key];
}

@end
