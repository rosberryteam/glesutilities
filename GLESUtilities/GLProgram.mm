//
//  GLProgram.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 4/20/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLProgram.h"
#import "GLDebug.h"
#import "GLLog.h"
#include "GLMatrix4x4.hpp"

typedef void (*GLInfoFunction)(GLuint program, GLenum pname, GLint* params);
typedef void (*GLLogFunction) (GLuint program, GLsizei bufsize, GLsizei* length, GLchar* infolog);

@interface GLProgram()

@property (nonatomic) NSMutableArray* attributes;
@property (nonatomic) NSMutableDictionary* uniformsCache;
@property (nonatomic) GLuint program, vertShader, fragShader;
@property (nonatomic) GLContext* context;

@end

@implementation GLProgram

+ (instancetype)program:(GLContext *)context
{
    [NSException raise:NSInternalInconsistencyException format:@"Need override %s in subclass!", __func__];
    return nil;
}

- (instancetype)initWithVertexShaderString:(NSString *)vertex fragmentShaderString:(NSString *)fragment context:(GLContext *)context
{
    self = [super init];
    if (self)
    {
        NSParameterAssert(vertex);
        NSParameterAssert(fragment);
        NSParameterAssert(context);
        
        self.context = context;
        self.attributes = [NSMutableArray array];
        self.uniformsCache = [NSMutableDictionary dictionary];
        
        self.program = glCreateProgram();
        
        if ([self compileShader:&_vertShader type:GL_VERTEX_SHADER string:vertex] == NO)
        {
            GLLog(GLLogVerboseLevelError, @"Failed to compile vertex shader");
        }
        
        if ([self compileShader:&_fragShader type:GL_FRAGMENT_SHADER string:fragment] == NO)
        {
            GLLog(GLLogVerboseLevelError, @"Failed to compile fragment shader");
        }
        
        glAttachShader(self.program, self.vertShader);
        glAttachShader(self.program, self.fragShader);
    }
    return self;
}

- (instancetype)initWithVertexShaderFileName:(NSString *)vertexFile fragmentShaderFileName:(NSString *)fragmentFile context:(GLContext *)context
{
    NSString* vertShaderPath = [[NSBundle mainBundle] pathForResource:vertexFile ofType:@"vsh"];
    NSString* vertexShaderString = [NSString stringWithContentsOfFile:vertShaderPath encoding:NSUTF8StringEncoding error:nil];
    
    NSString* fragShaderPath = [[NSBundle mainBundle] pathForResource:fragmentFile ofType:@"fsh"];
    NSString* fragmentShaderString = [NSString stringWithContentsOfFile:fragShaderPath encoding:NSUTF8StringEncoding error:nil];
    
    return [self initWithVertexShaderString:vertexShaderString fragmentShaderString:fragmentShaderString context:context];
}

- (void)dealloc
{
    if (_vertShader || _fragShader || _program)
    {
        [self.context lock:^{ [self cleanup]; }];
    }
}

#pragma mark -
#pragma mark Compile

- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type string:(NSString *)shaderString
{
    GLint status;
    const GLchar* source = (GLchar*)[shaderString UTF8String];
    
    if (!source)
    {
        GLLog(GLLogVerboseLevelError, @"Failed to load vertex shader");
        return NO;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    
    if (status != GL_TRUE)
    {
        GLint logLength;
        glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
        if (logLength > 0)
        {
            GLchar *log = (GLchar *)malloc(logLength);
            glGetShaderInfoLog(*shader, logLength, &logLength, log);
            GLLog(GLLogVerboseLevelError, @"Shader compile log:\n%s", log);
            free(log);
        }
    }
    
    return status == GL_TRUE;
}

#pragma mark -
#pragma mark Attrib / Uniforms

- (void)addAttribute:(NSString *)attributeName
{
    if (![self.attributes containsObject:attributeName])
    {
        [self.attributes addObject:attributeName];
        glBindAttribLocation(self.program, (GLuint)[self.attributes indexOfObject:attributeName], [attributeName UTF8String]);
    }
}

- (GLuint)attributeIndex:(NSString *)attributeName
{
    return (GLuint)[self.attributes indexOfObject:attributeName];
}

- (GLuint)uniformIndex:(NSString *)uniformName
{
    return glGetUniformLocation(self.program, [uniformName UTF8String]);
}

#pragma mark -
#pragma mark Link & Use

- (BOOL)link
{
    GLint status;
    
    glLinkProgram(self.program);
    
    glGetProgramiv(self.program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        return NO;
    }
    
    if (self.vertShader)
    {
        glDeleteShader(self.vertShader);
        self.vertShader = 0;
    }
    
    if (self.fragShader)
    {
        glDeleteShader(self.fragShader);
        self.fragShader = 0;
    }
    
    _initialized = YES;
    
    return YES;
}

- (void)use
{
    glUseProgram(self.program);
}

#pragma mark -
#pragma mark Log & Validate

- (NSString *)logForOpenGLObject:(GLuint)object infoCallback:(GLInfoFunction)infoFunc logFunc:(GLLogFunction)logFunc
{
    GLint logLength = 0, charsWritten = 0;
    
    infoFunc(object, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength < 1)
    {
        return nil;
    }
    
    GLchar* logBytes = (GLchar *)malloc(logLength);
    logFunc(object, logLength, &charsWritten, logBytes);
    NSString* log = [[NSString alloc] initWithBytes:logBytes length:logLength encoding:NSUTF8StringEncoding];
    free(logBytes);
    return log;
}

- (NSString *)vertexShaderLog
{
    return [self logForOpenGLObject:self.vertShader infoCallback:(GLInfoFunction)&glGetProgramiv logFunc:(GLLogFunction)&glGetProgramInfoLog];
}

- (NSString *)fragmentShaderLog
{
    return [self logForOpenGLObject:self.fragShader infoCallback:(GLInfoFunction)&glGetProgramiv logFunc:(GLLogFunction)&glGetProgramInfoLog];
}

- (NSString *)programLog
{
    return [self logForOpenGLObject:self.program infoCallback:(GLInfoFunction)&glGetProgramiv logFunc:(GLLogFunction)&glGetProgramInfoLog];
}

- (void)validate
{
    GLint logLength;
    
    glValidateProgram(self.program);
    glGetProgramiv(self.program, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar* log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(self.program, logLength, &logLength, log);
        GLLog(GLLogVerboseLevelWarning, @"Program validate log:\n%s", log);
        free(log);
    }
}

#pragma mark -
#pragma mark Set Uniforms

- (void)setUniform1f:(GLint)location value:(GLfloat)value
{
#if USE_STATE_CACHE
    NSNumber* number = self.uniformsCache[@(location)];
    if (!number || ![number isKindOfClass:[NSNumber class]] || [number floatValue] != value)
    {
#endif
        glUniform1f(location, value);
#if USE_STATE_CACHE
        self.uniformsCache[@(location)] = @(value);
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; };
#endif
}

- (void)setUniform1i:(GLint)location value:(GLint)value
{
#if USE_STATE_CACHE
    NSNumber* number = self.uniformsCache[@(location)];
    if (!number || ![number isKindOfClass:[NSNumber class]] || [number integerValue] != value)
    {
#endif
        glUniform1i(location, value);
#if USE_STATE_CACHE
        self.uniformsCache[@(location)] = @(value);
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; };
#endif
}

- (void)setUniform2f:(GLint)location value:(CGPoint)point
{
#if USE_STATE_CACHE
    NSValue* value = self.uniformsCache[@(location)];
    if (!value || ![value isKindOfClass:[NSValue class]] || !CGPointEqualToPoint(point, [value CGPointValue]))
    {
#endif
        glUniform2f(location, point.x, point.y);
#if USE_STATE_CACHE
        self.uniformsCache[@(location)] = [NSValue valueWithCGPoint:point];
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; };
#endif
}

- (void)setUniformMatrix4x4:(GLint)location matrix:(const GLMatrix4x4 &)matrix
{
#if USE_STATE_CACHE
    NSData* data = self.uniformsCache[@(location)];
    if (!data || ![[data class] isSubclassOfClass:[NSData class]] || memcmp(data.bytes, matrix.pointer(), sizeof(float) * 16) != 0)
    {
#endif
        glUniformMatrix4fv(location, 1, GL_FALSE, matrix.pointer());
#if USE_STATE_CACHE
        self.uniformsCache[@(location)] = [NSData dataWithBytes:matrix.pointer() length:(sizeof(float) * 16)];
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; };
#endif
}

#pragma mark -
#pragma mark GLCleanupProtocol

- (void)cleanup
{
    if (_vertShader)
    {
        glDeleteShader(_vertShader);
        _vertShader = 0;
    }
    
    if (_fragShader)
    {
        glDeleteShader(_fragShader);
        _fragShader = 0;
    }
    
    if (_program)
    {
        glDeleteProgram(_program);
        _program = 0;
    }
}

@end
