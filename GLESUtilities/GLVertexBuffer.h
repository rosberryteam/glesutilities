//
//  GLVertexBuffer.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 5/8/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLContext.h"
#import "GLCleanupProtocol.h"

@interface GLVertexBuffer : NSObject <GLContext, GLCleanupProtocol>

@property (nonatomic,readonly) GLuint handle;
@property (nonatomic,readonly) NSUInteger dataSize;

+ (instancetype)vertexBufferWithDataSize:(NSUInteger)size context:(GLContext *)context;

- (void)bind;
- (void)unbind;

- (BOOL)map:(void(^)(void* data))block;

@end
