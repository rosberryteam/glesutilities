//
//  GLProgramCache.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 6/9/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GLContext;
@class GLProgram;

@interface GLProgramCache : NSObject

+ (id)programWithClass:(Class)programClass context:(GLContext *)context;

+ (void)cacheProgram:(GLProgram *)program;
+ (void)contextDidDestroy:(GLContext *)context;

@end
