//
//  GLContext.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 03/05/2013.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLESUtilities.h"

@interface GLContext ()

@property (nonatomic) dispatch_semaphore_t semaphore;
@property (nonatomic) EAGLContext* eaglContext;

@property (nonatomic) BOOL locked;
@property (nonatomic,weak) NSThread* lockedThread;

@end

@implementation GLContext

+ (NSArray *)extensions
{
    static NSArray* extensions = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [[self sharedContext] set];
        NSString* string = [NSString stringWithCString:(const char *)glGetString(GL_EXTENSIONS) encoding:NSUTF8StringEncoding];
        extensions = [string componentsSeparatedByString:@" "];
    });
    return extensions;
}

+ (BOOL)supportExtension:(NSString *)extension
{
    return [self.extensions containsObject:extension];
}

+ (BOOL)supportExtensionTextureRG
{
    return [self supportExtension:@"GL_EXT_texture_rg"];
}

+ (BOOL)supportDiscardFramebuffer
{
    return [self supportExtension:@"GL_EXT_discard_framebuffer"];
}

+ (GLint)maximumTextureSize
{
    static GLint maxTextureSize = 0;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        [[self sharedContext] set];
        glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize);
    });
    return maxTextureSize;
}

+ (CGSize)sizeThatFitInMaxSupportSize:(CGSize)inputSize
{
    GLfloat maxTextureSize = [GLContext maximumTextureSize];
    if ((inputSize.width < maxTextureSize) && (inputSize.height < maxTextureSize))
    {
        return CGSizeMake((int)inputSize.width, (int)inputSize.height);
    }
    
    CGSize adjustedSize;
    if (inputSize.width > inputSize.height)
    {
        adjustedSize.width = (int)maxTextureSize;
        adjustedSize.height = (int)((maxTextureSize / inputSize.width) * inputSize.height);
    }
    else
    {
        adjustedSize.height = (int)maxTextureSize;
        adjustedSize.width = (int)((maxTextureSize / inputSize.height) * inputSize.width);
    }
    
    GLLog(GLLogVerboseLevelError, @"WARNING: size bigger that device support, check new size! inputSize - %@, adjustedSize - %@", NSStringFromCGSize(inputSize), NSStringFromCGSize(adjustedSize));
    
    return adjustedSize;
}

+ (CGFloat)scale
{
    return [UIScreen mainScreen].scale;
}

+ (GLContext *)sharedContext
{
    static GLContext* sharedContext = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedContext = [[self alloc] initWithSharedContext:nil];
    });
    return sharedContext;
}

+ (GLContext *)context
{
    return [[self alloc] initWithSharedContext:nil];
}

+ (GLContext *)contextWithSharedContext:(GLContext *)context
{
    NSParameterAssert(context);
    return [[self alloc] initWithSharedContext:context];
}

- (id)initWithSharedContext:(GLContext *)context
{
    self = [super init];
    if (self)
    {
        _semaphore = dispatch_semaphore_create(1);
        
        [self lock];
        
        if (context)
        {
            self.eaglContext = [[EAGLContext alloc] initWithAPI:context.eaglContext.API sharegroup:context.eaglContext.sharegroup];
        }
        else
        {
            self.eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        }
        
        if ([self.eaglContext respondsToSelector:@selector(setMultiThreaded:)])
        {
            self.eaglContext.multiThreaded = YES;
        }
        
        CVReturn err = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, self.eaglContext, NULL, &_videoTextureCache);
        if (err)
        {
            GLLog(GLLogVerboseLevelError, @"Error at CVOpenGLESTextureCacheCreate %d", err);
        }
        
        [self unlock];
    }
    return self;
}

- (void)dealloc
{
    [GLProgramCache contextDidDestroy:self];
    [GLStateCache contextDidDestroy:self];
    
    GLLog(GLLogVerboseLevelWarning, @"%s", __func__);
    
    if (_videoTextureCache)
    {
        CFRelease(_videoTextureCache);
    }
}

#pragma mark -
#pragma mark Lock / Unlock Context

- (void)set
{
    if ([EAGLContext currentContext] != self.eaglContext)
    {
        [EAGLContext setCurrentContext:self.eaglContext];
    }
}

- (void)lock
{
    if (self.locked && self.lockedThread == [NSThread currentThread])
    {
        [self unlock];
        [NSException raise:NSInternalInconsistencyException format:@"Lock already locked GLContext in same thread may cause deadlock, check your code on correct call -lock/unlock!"];
    }
    
    NSDate* date = nil;
    while ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
    {
        if (date == nil)
        {
            date = [NSDate dateWithTimeIntervalSinceNow:0.1];
        }
        
        [NSThread sleepUntilDate:date];
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:date];
        date = [NSDate dateWithTimeIntervalSinceNow:0.1];
    }
    
    dispatch_semaphore_wait(_semaphore, DISPATCH_TIME_FOREVER);
    [self set];
    
    self.locked = YES;
    self.lockedThread = [NSThread currentThread];
}

- (void)unlock
{
    self.locked = NO;
    self.lockedThread = nil;
    
    dispatch_semaphore_signal(_semaphore);
}

- (void)lock:(dispatch_block_t)block
{
    [self lock];
    {
        block();
    }
    [self unlock];
}

#pragma mark -
#pragma mark Drawable Additions

- (BOOL)renderbufferStorage:(NSUInteger)target fromDrawable:(id<EAGLDrawable>)drawable
{
    return [self.eaglContext renderbufferStorage:target fromDrawable:drawable];
}

- (BOOL)presentRenderbuffer:(NSUInteger)target
{
    return [self.eaglContext presentRenderbuffer:target];
}

@end
