//
//  GLCommon.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 4/20/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#ifndef __GLESUTILITIES_GLCOMMON_H__
#define __GLESUTILITIES_GLCOMMON_H__

#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>

#endif
