//
//  GLStateCache.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 4/8/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLStateCache.h"
#import "GLProgram.h"

#if PROFILE_STATE_CACHE
static long long __redundantCall = 0;
#endif

@interface GLStateCacheObject : NSObject <GLStateCache>

@property (nonatomic) CGSize viewPortSize;
@property (nonatomic) GLfloat red, green, blue, alpha;
@property (nonatomic) GLuint framebufferTarget, framebufferHandle;
@property (nonatomic) GLuint renderbufferTarget, renderbufferHandle;
@property (nonatomic, unsafe_unretained) GLProgram* program;

@end

@implementation GLStateCacheObject

- (void)reset
{
    self.viewPortSize = CGSizeZero;
    self.red = self.green = self.blue = self.alpha = 0.0f;
    self.framebufferTarget = self.framebufferHandle = 0;
    self.renderbufferTarget = self.renderbufferHandle = 0;
    self.program = nil;
}

- (void)setViewportSize:(CGSize)size
{
#if USE_STATE_CACHE
    if (!CGSizeEqualToSize(size, self.viewPortSize))
    {
        self.viewPortSize = size;
#endif
        glViewport(0, 0, (GLsizei)size.width, (GLsizei)size.height);
#if USE_STATE_CACHE
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; }
#endif
}

- (void)setClearColorRed:(CGFloat)red green:(GLfloat)green blue:(GLfloat)blue alpha:(GLfloat)alpha
{
#if USE_STATE_CACHE
    if (self.red != red || self.green != green || self.blue != blue || self.alpha != alpha)
    {
        self.red = red;
        self.green = green;
        self.blue = blue;
        self.alpha = alpha;
#endif
        glClearColor(red, green, blue, alpha);
#if USE_STATE_CACHE
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; }
#endif
}

- (void)useGLProgram:(GLProgram *)program
{
#if USE_STATE_CACHE
    if (self.program != program)
    {
        self.program = program;
#endif
        [program use];
#if USE_STATE_CACHE
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; }
#endif
}

- (void)bindFramebuffer:(GLuint)target framebuffer:(GLuint)framebuffer
{
#if USE_STATE_CACHE
    if (self.framebufferTarget != target || self.framebufferHandle != framebuffer)
    {
        self.framebufferTarget = target;
        self.framebufferHandle = framebuffer;
#endif
        glBindFramebuffer(target, framebuffer);
#if USE_STATE_CACHE
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; }
#endif
}

- (void)bindRenderbuffer:(GLuint)target framebuffer:(GLuint)renderbuffer
{
#if USE_STATE_CACHE
    if (self.renderbufferTarget != target || self.renderbufferHandle != renderbuffer)
    {
        self.renderbufferTarget = target;
        self.renderbufferHandle = renderbuffer;
#endif
        glBindRenderbuffer(target, renderbuffer);
#if USE_STATE_CACHE
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; }
#endif
}

@end

#pragma mark -
#pragma mark GLStateCache

@interface GLStateCache ()

@property (nonatomic) NSMutableDictionary* caches;

@end

@implementation GLStateCache

+ (instancetype)sharedInstance
{
    static GLStateCache* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.caches = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)beginWithContext:(GLContext *)context block:(void(^)(id<GLStateCache> cache))block
{
    NSValue* key = [NSValue valueWithNonretainedObject:context];
    if (!self.caches[key])
    {
        self.caches[key] = [[GLStateCacheObject alloc] init];
    }
    
    block(self.caches[key]);
}

- (void)contextDidDestroy:(GLContext *)context
{
    NSValue* key = [NSValue valueWithNonretainedObject:context];
    if (self.caches[key])
    {
        [self.caches removeObjectForKey:key];
    }
}

#pragma mark -
#pragma mark Class Methods

+ (void)beginWithContext:(GLContext *)context block:(void(^)(id<GLStateCache> cache))block
{
    [[self sharedInstance] beginWithContext:context block:block];
}

+ (void)contextDidDestroy:(GLContext *)context
{
    [[self sharedInstance] contextDidDestroy:context];
}

#if PROFILE_STATE_CACHE
+ (void)incRedundantCall
{
    __redundantCall++;
}

+ (void)showStatistics
{
    NSLog(@"Redundant Call save by State Cache: %lld",__redundantCall);
}
#endif

@end
