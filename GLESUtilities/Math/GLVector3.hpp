//
//  GLVector3.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 15/06/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#ifndef __GLESUTILITIES__GLVECTOR3___
#define __GLESUTILITIES__GLVECTOR3___

#ifdef __cplusplus

class GLVector3
{
    friend class GLMatrix4x4;
    
    float v[3];
    
public:
    
    GLVector3(float x = 0.0f, float y = 0.0f, float z = 0.0f);
    ~GLVector3();
    
    float length();
    
    GLVector3 negative();
    GLVector3 normalize();
    
    static GLVector3 Add(GLVector3 left, GLVector3 right);
    static GLVector3 CrossProduct(GLVector3 left, GLVector3 right);
    static float DotProduct(GLVector3 left, GLVector3 right);
};

#endif /* __cplusplus */
#endif /* defined(__GLESUTILITIES__GLVECTOR3___) */
