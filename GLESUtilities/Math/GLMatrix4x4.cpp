//
//  GLMatrix4x4.cpp
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 5/30/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#include "GLMatrix4x4.hpp"
#include "GLVector3.hpp"
#include <algorithm>
#include <math.h>

#if defined(__ARM_NEON__)
#include <arm_neon.h>
#endif

static const float _m_identity[16] =
{
    1.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 1.0f
};

GLMatrix4x4::GLMatrix4x4()
{
    std::copy(_m_identity, _m_identity + 16, m);
}

GLMatrix4x4::GLMatrix4x4(const GLMatrix4x4& right)
{
    for (int i = 0; i < 16; i++) m[i] = right.m[i];
}

GLMatrix4x4::~GLMatrix4x4()
{
    
}

GLMatrix4x4 GLMatrix4x4::Ortho(float left, float right, float bottom, float top, float nearZ, float farZ)
{
    GLMatrix4x4 matrix;
    
    float ral = right + left;
    float rsl = right - left;
    float tab = top + bottom;
    float tsb = top - bottom;
    float fan = farZ + nearZ;
    float fsn = farZ - nearZ;
    
    float* m = matrix.m;
    
    m[0] = 2.0f / rsl;  m[4] = 0.0f;        m[8] = 0.0f;            m[12] = -ral / rsl;
    m[1] = 0.0f;        m[5] = 2.0f / tsb;  m[9] = 0.0f;            m[13] = -tab / tsb;
    m[2] = 0.0f;        m[6] = 0.0f;        m[10] = -2.0f / fsn;    m[14] = -fan / fsn;
    m[3] = 0.0f;        m[7] = 0.0f;        m[11] = 0.0f;           m[15] = 1.0f;
    
    return matrix;
}

GLMatrix4x4 GLMatrix4x4::Frustum(float left, float right, float bottom, float top, float nearZ, float farZ)
{
    GLMatrix4x4 matrix;
    
    float ral = right + left;
    float rsl = right - left;
    float tsb = top - bottom;
    float tab = top + bottom;
    float fan = farZ + nearZ;
    float fsn = farZ - nearZ;
    
    float* m = matrix.m;
    
    m[0] = 2.0f * nearZ / rsl;  m[4] = 0.0f;                m[8] = ral / rsl;       m[12] = 0.0f;
    m[1] = 0.0f;                m[5] = 2.0f * nearZ / tsb;  m[9] = tab / tsb;       m[13] = 0.0f;
    m[2] = 0.0f;                m[6] = 0.0f;                m[10] = -fan / fsn;     m[14] = (-2.0f * farZ * nearZ) / fsn;
    m[3] = 0.0f;                m[7] = 0.0f;                m[11] = -1.0f;          m[15] = 0.0f;
    
    return matrix;
}

GLMatrix4x4 GLMatrix4x4::Perspective(float fovyRadians, float aspect, float nearZ, float farZ)
{
    GLMatrix4x4 matrix;
    
    float cotan = 1.0f / tanf(fovyRadians / 2.0f);
    
    float* m = matrix.m;
    
    m[0] = cotan / aspect;  m[4] = 0.0f;    m[8] = 0.0f;                                m[12] = 0.0f;
    m[1] = 0.0f;            m[5] = cotan;   m[9] = 0.0f;                                m[13] = 0.0f;
    m[2] = 0.0f;            m[6] = 0.0f;    m[10] = (farZ + nearZ) / (nearZ - farZ);    m[14] = (2.0f * farZ * nearZ) / (nearZ - farZ),
    m[3] = 0.0f;            m[7] = 0.0f;    m[11] = -1.0;                               m[15] = 0.0f;
    
    return matrix;
}


GLMatrix4x4 GLMatrix4x4::LookAt(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ)
{
    GLVector3 eye = GLVector3(eyeX, eyeY, eyeZ);
    GLVector3 center = GLVector3(centerX, centerY, centerZ);
    GLVector3 up = GLVector3(upX, upY, upZ);
    
    GLVector3 n = GLVector3::Add(eye, center.negative()).normalize();
    GLVector3 u = GLVector3::CrossProduct(up, n).normalize();
    GLVector3 v = GLVector3::CrossProduct(n, u);
    
    GLMatrix4x4 matrix;
    float* m = matrix.m;
    
    m[0] = u.v[0];  m[4] = u.v[1];  m[8] = u.v[2];  m[12] = GLVector3::DotProduct(u.negative(), eye);
    m[1] = v.v[0];  m[5] = v.v[1];  m[9] = v.v[2];  m[13] = GLVector3::DotProduct(v.negative(), eye);
    m[2] = n.v[0];  m[6] = n.v[1];  m[10] = n.v[2]; m[14] = GLVector3::DotProduct(n.negative(), eye);
    m[3] = 0.0f;    m[7] = 0.0f;    m[11] = 0.0f;   m[15] = 1.0f;
    
    return matrix;
}

GLMatrix4x4 GLMatrix4x4::Translate(float x, float y, float z)
{
    GLMatrix4x4 matrix;
    matrix.m[12] = x;
    matrix.m[13] = y;
    matrix.m[14] = z;
    return matrix;
}

GLMatrix4x4 GLMatrix4x4::RotationX(float radians)
{
    float cos = cosf(radians);
    float sin = sinf(radians);
    
    GLMatrix4x4 matrix;
    matrix.m[5] = cos;
    matrix.m[6] = sin;
    matrix.m[9] = -sin;
    matrix.m[10] = cos;
    return matrix;
}

GLMatrix4x4 GLMatrix4x4::RotationY(float radians)
{
    float cos = cosf(radians);
    float sin = sinf(radians);
    
    GLMatrix4x4 matrix;
    matrix.m[0] = cos;
    matrix.m[2] = -sin;
    matrix.m[8] = sin;
    matrix.m[10] = cos;
    return matrix;
}

GLMatrix4x4 GLMatrix4x4::RotationZ(float radians)
{
    float cos = cosf(radians);
    float sin = sinf(radians);
    
    GLMatrix4x4 matrix;
    matrix.m[0] = cos;
    matrix.m[1] = sin;
    matrix.m[4] = -sin;
    matrix.m[5] = cos;
    return matrix;
}

GLMatrix4x4 GLMatrix4x4::Scale(float scale)
{
    GLMatrix4x4 matrix;
    matrix.m[0] = scale;
    matrix.m[5] = scale;
    matrix.m[10] = scale;
    return matrix;
}

GLMatrix4x4 GLMatrix4x4::Scale(float x, float y, float z)
{
    GLMatrix4x4 matrix;
    matrix.m[0] = x;
    matrix.m[5] = y;
    matrix.m[10] = z;
    return matrix;
}

void GLMatrix4x4::setFOV(float fov)
{
    m[11] = fov;
}

const float* GLMatrix4x4::pointer() const
{
    return m;
}

GLMatrix4x4& GLMatrix4x4::operator*= (const GLMatrix4x4& right)
{
#if defined(__ARM_NEON__)
    float32x4x4_t iMatrixLeft = *(float32x4x4_t *)m;
    float32x4x4_t iMatrixRight = *(float32x4x4_t *)right.m;
    float32x4x4_t tmp;
    
    tmp.val[0] = vmulq_n_f32(iMatrixLeft.val[0], vgetq_lane_f32(iMatrixRight.val[0], 0));
    tmp.val[1] = vmulq_n_f32(iMatrixLeft.val[0], vgetq_lane_f32(iMatrixRight.val[1], 0));
    tmp.val[2] = vmulq_n_f32(iMatrixLeft.val[0], vgetq_lane_f32(iMatrixRight.val[2], 0));
    tmp.val[3] = vmulq_n_f32(iMatrixLeft.val[0], vgetq_lane_f32(iMatrixRight.val[3], 0));
    
    tmp.val[0] = vmlaq_n_f32(tmp.val[0], iMatrixLeft.val[1], vgetq_lane_f32(iMatrixRight.val[0], 1));
    tmp.val[1] = vmlaq_n_f32(tmp.val[1], iMatrixLeft.val[1], vgetq_lane_f32(iMatrixRight.val[1], 1));
    tmp.val[2] = vmlaq_n_f32(tmp.val[2], iMatrixLeft.val[1], vgetq_lane_f32(iMatrixRight.val[2], 1));
    tmp.val[3] = vmlaq_n_f32(tmp.val[3], iMatrixLeft.val[1], vgetq_lane_f32(iMatrixRight.val[3], 1));
    
    tmp.val[0] = vmlaq_n_f32(tmp.val[0], iMatrixLeft.val[2], vgetq_lane_f32(iMatrixRight.val[0], 2));
    tmp.val[1] = vmlaq_n_f32(tmp.val[1], iMatrixLeft.val[2], vgetq_lane_f32(iMatrixRight.val[1], 2));
    tmp.val[2] = vmlaq_n_f32(tmp.val[2], iMatrixLeft.val[2], vgetq_lane_f32(iMatrixRight.val[2], 2));
    tmp.val[3] = vmlaq_n_f32(tmp.val[3], iMatrixLeft.val[2], vgetq_lane_f32(iMatrixRight.val[3], 2));
    
    tmp.val[0] = vmlaq_n_f32(tmp.val[0], iMatrixLeft.val[3], vgetq_lane_f32(iMatrixRight.val[0], 3));
    tmp.val[1] = vmlaq_n_f32(tmp.val[1], iMatrixLeft.val[3], vgetq_lane_f32(iMatrixRight.val[1], 3));
    tmp.val[2] = vmlaq_n_f32(tmp.val[2], iMatrixLeft.val[3], vgetq_lane_f32(iMatrixRight.val[2], 3));
    tmp.val[3] = vmlaq_n_f32(tmp.val[3], iMatrixLeft.val[3], vgetq_lane_f32(iMatrixRight.val[3], 3));
    
    memcpy(m, &tmp, sizeof(float) * 16);
#else
    float* tmp = new float[16];
    
    tmp[0]  = m[0] * right.m[0]  + m[4] * right.m[1]  + m[8] * right.m[2]   + m[12] * right.m[3];
	tmp[4]  = m[0] * right.m[4]  + m[4] * right.m[5]  + m[8] * right.m[6]   + m[12] * right.m[7];
	tmp[8]  = m[0] * right.m[8]  + m[4] * right.m[9]  + m[8] * right.m[10]  + m[12] * right.m[11];
	tmp[12] = m[0] * right.m[12] + m[4] * right.m[13] + m[8] * right.m[14]  + m[12] * right.m[15];
    
	tmp[1]  = m[1] * right.m[0]  + m[5] * right.m[1]  + m[9] * right.m[2]   + m[13] * right.m[3];
	tmp[5]  = m[1] * right.m[4]  + m[5] * right.m[5]  + m[9] * right.m[6]   + m[13] * right.m[7];
	tmp[9]  = m[1] * right.m[8]  + m[5] * right.m[9]  + m[9] * right.m[10]  + m[13] * right.m[11];
	tmp[13] = m[1] * right.m[12] + m[5] * right.m[13] + m[9] * right.m[14]  + m[13] * right.m[15];
    
	tmp[2]  = m[2] * right.m[0]  + m[6] * right.m[1]  + m[10] * right.m[2]  + m[14] * right.m[3];
	tmp[6]  = m[2] * right.m[4]  + m[6] * right.m[5]  + m[10] * right.m[6]  + m[14] * right.m[7];
	tmp[10] = m[2] * right.m[8]  + m[6] * right.m[9]  + m[10] * right.m[10] + m[14] * right.m[11];
	tmp[14] = m[2] * right.m[12] + m[6] * right.m[13] + m[10] * right.m[14] + m[14] * right.m[15];
    
	tmp[3]  = m[3] * right.m[0]  + m[7] * right.m[1]  + m[11] * right.m[2]  + m[15] * right.m[3];
	tmp[7]  = m[3] * right.m[4]  + m[7] * right.m[5]  + m[11] * right.m[6]  + m[15] * right.m[7];
	tmp[11] = m[3] * right.m[8]  + m[7] * right.m[9]  + m[11] * right.m[10] + m[15] * right.m[11];
	tmp[15] = m[3] * right.m[12] + m[7] * right.m[13] + m[11] * right.m[14] + m[15] * right.m[15];
    
    std::copy(tmp, tmp + 16, m);
    delete [] tmp;
#endif
    return *this;
}

GLMatrix4x4& GLMatrix4x4::operator* (const GLMatrix4x4& right)
{
    return *this *= right;
}

GLMatrix4x4& GLMatrix4x4::operator= (const GLMatrix4x4& right)
{
    for (int i = 0; i < 16; i++) m[i] = right.m[i];
    return *this;
}

bool GLMatrix4x4::operator== (const GLMatrix4x4& right)
{
    bool result = true;
    for (int i = 0; i < 16; i++)
    {
        if (m[i] != right.m[i]) result = false;
    }
    return result;
}

bool GLMatrix4x4::operator!= (const GLMatrix4x4& right)
{
    bool result = true;
    for (int i = 0; i < 16; i++)
    {
        if (m[i] != right.m[i]) result = false;
    }
    return !result;
}
