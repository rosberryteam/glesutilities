//
//  GLVertexBuffer.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 5/8/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLVertexBuffer.h"
#import "GLDebug.h"

@interface GLVertexBuffer ()

@property (nonatomic) GLContext* context;

@end

@implementation GLVertexBuffer

+ (instancetype)vertexBufferWithDataSize:(NSUInteger)size context:(GLContext *)context
{
    return [[self alloc] initWithDataSize:size context:context];
}

- (instancetype)initWithDataSize:(NSUInteger)size context:(GLContext *)context
{
    self = [super init];
    if (self)
    {
        NSAssert(size != 0, @"Data size must be > 0");
        NSParameterAssert(context);
        
        _context = context;
        _dataSize = size;
        
        void* data = calloc(1, size);
        NSAssert(data, @"Failed allocate memory for size %zd", size);
        
        glGenBuffers(1, &_handle);
        
        NSAssert(_handle != 0, @"Failed create VBO");
        
        glBindBuffer(GL_ARRAY_BUFFER, _handle);
        glBufferData(GL_ARRAY_BUFFER, size, data, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        free(data);
    }
    return self;
}

- (void)dealloc
{
    if (_handle)
    {
        [self.context lock:^{ [self cleanup]; }];
    }
}

#pragma mark -
#pragma mark Bind/Unbind

- (void)bind
{
    glBindBuffer(GL_ARRAY_BUFFER, _handle);
}

- (void)unbind
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

#pragma mark -
#pragma mark Map Vertex

- (BOOL)map:(void(^)(void* data))block
{
    void* data = glMapBufferOES(GL_ARRAY_BUFFER, GL_WRITE_ONLY_OES);
    if (data)
    {
        block(data);
        glUnmapBufferOES(GL_ARRAY_BUFFER);
        return YES;
    }
    else
    {
        return NO;
    }
}

#pragma mark -
#pragma mark GLCleanupProtocol

- (void)cleanup
{
    if (_handle)
    {
        glDeleteBuffers(1, &_handle);
        _handle = 0;
    }
}

#pragma mark -

- (NSString *)description
{
    [self.context set];
    
    NSMutableString* string = [NSMutableString stringWithFormat:@"<%@: %p> vboHandle = %zd, size = %zd, data:\n", [self class], self, self.handle, self.dataSize];
    
    void (^block)() = ^(void *data)
    {
        for (NSInteger i = 0; i < self.dataSize / sizeof(float); i += 2)
        {
            float* p = (float *)data + i;
            [string appendFormat:@"{%.2f, %.2f}\n", p[0], p[1]];
        }
    };
    
    [self bind];
    if (![self map:block])
    {
        [string appendString:@"Failed to map buffer!"];
    }
    [self unbind];
    
    return [NSString stringWithString:string];
}

@end
