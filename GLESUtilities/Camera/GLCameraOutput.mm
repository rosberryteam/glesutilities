//
//  GLCameraOutput.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 3/26/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLCameraOutput.h"
#import "GLCameraOutputProgram.h"
#import "GLDebug.h"
#import "GLFrameBuffer.h"
#import "GLLog.h"
#import "GLStateCache.h"
#import "GLVertexArray.h"

#include <sys/sysctl.h>
#include <sys/utsname.h>

NSInteger const GLCameraOutputCantAddInputDeviceErrorCode = 100;
NSInteger const GLCameraOutputFailedInitializeAVCaptureSessionErrorCode = 101;

NS_INLINE CGRect GLCameraOutputRectByCalculateAspectOfSizes(CGSize outSize, CGSize inputSize)
{
    CGFloat aspect = MAX(outSize.width / inputSize.width, outSize.height / inputSize.height);
    inputSize.width *= aspect;
    inputSize.height *= aspect;
    return CGRectMake(-(inputSize.width - outSize.width), -(inputSize.height - outSize.height), outSize.width, outSize.height);
}

NS_INLINE NSString* GLCameraOutputPlatformString()
{
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char* machine = (char *)malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString* platform = [NSString stringWithUTF8String:machine];
    free(machine);
    return platform;
}

AVCaptureVideoOrientation GLCameraOutputAVCaptureVideoOrientationFromUIInterfaceOrientation(UIInterfaceOrientation orientation)
{
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:            return AVCaptureVideoOrientationPortrait;
        case UIInterfaceOrientationPortraitUpsideDown:  return AVCaptureVideoOrientationPortraitUpsideDown;
        case UIInterfaceOrientationLandscapeLeft:       return AVCaptureVideoOrientationLandscapeLeft;
        case UIInterfaceOrientationLandscapeRight:      return AVCaptureVideoOrientationLandscapeRight;
        default: return AVCaptureVideoOrientationPortrait;
    }
}


@interface GLCameraOutput ()
{
    CVOpenGLESTextureRef _lumaTexture;
    CVOpenGLESTextureRef _chromaTexture;
    dispatch_queue_t _camera_output_queue;
}

@property (nonatomic) GLContext* context;
@property (nonatomic) GLCameraOutputProgram* cameraOutputProgram;
@property (nonatomic) GLFrameBuffer* fbo;
@property (nonatomic) GLVertexArray* vertexArray;

@property (nonatomic) AVCaptureSession* session;
@property (nonatomic) AVCaptureDevice* captureDevice;
@property (nonatomic) AVCaptureDeviceInput* captureDeviceInput;
@property (nonatomic) NSString* captureSessionPreset;
@property (nonatomic) BOOL supportsFullYUVRange;
@property (nonatomic) CGSize outputSize;

@end

@implementation GLCameraOutput

+ (AVAuthorizationStatus)authorizationStatus
{
    return [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
}

+ (void)requestAccessWithCompletionHandler:(void (^)(BOOL granted))handler
{
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:handler];
}

+ (AVCaptureDevice *)captureDeviceWithPosition:(AVCaptureDevicePosition)position
{
    for (AVCaptureDevice* device in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo])
    {
        if (device.position == position)
        {
            return device;
        }
    }
    return nil;
}

+ (BOOL)backCameraAvailable
{
    return [self captureDeviceWithPosition:AVCaptureDevicePositionBack] != nil;
}

+ (BOOL)frontCameraAvailable
{
    return [self captureDeviceWithPosition:AVCaptureDevicePositionFront] != nil;
}

+ (BOOL)canFlipCamera
{
    return [self frontCameraAvailable] && [self backCameraAvailable];
}

+ (AVCaptureDevicePosition)defaultCaptureDevicePosition
{
    if ([self backCameraAvailable])
    {
        return AVCaptureDevicePositionBack;
    }
    else if ([self frontCameraAvailable])
    {
        return AVCaptureDevicePositionFront;
    }
    return AVCaptureDevicePositionUnspecified;
}

#pragma mark -

+ (instancetype)cameraWithSize:(CGSize)size context:(GLContext *)context error:(NSError **)error
{
    return [[self alloc] initWithSize:size context:context captureSessionPreset:AVCaptureSessionPresetPhoto error:error];
}

+ (instancetype)cameraWithSize:(CGSize)size context:(GLContext *)context captureSessionPreset:(NSString *)captureSessionPreset error:(NSError **)error
{
    return [[self alloc] initWithSize:size context:context captureSessionPreset:captureSessionPreset error:error];
}

- (instancetype)initWithSize:(CGSize)size context:(GLContext *)context captureSessionPreset:(NSString *)captureSessionPreset error:(NSError **)error
{
    self = [super init];
    if (self)
    {
        NSParameterAssert(context);
        
        _context = context;
        _videoOrientation = AVCaptureVideoOrientationPortrait;
        
        [self.context lock:^() {
            
            self.cameraOutputProgram = [GLCameraOutputProgram programWithSupportsFullYUVRange:self.supportsFullYUVRange context:self.context];
            self.vertexArray = [GLVertexArray vertexArrayWithDataSize:sizeof(GLfloat) * 16 context:self.context];
            [self.vertexArray setupVertexAttrib:self.cameraOutputProgram.attribPosition size:2 stride:sizeof(GLfloat) * 4 offset:0];
            [self.vertexArray setupVertexAttrib:self.cameraOutputProgram.attribTexCoord size:2 stride:sizeof(GLfloat) * 4 offset:sizeof(GLfloat) * 2];
        }];
        
        self.captureSessionPreset = [captureSessionPreset copy];
        self.scale = 1.0f;
        self.size = size;
        
        NSError* initializeSessionError = nil;
        if (![self initializeSession:&initializeSessionError])
        {
            GLLog(GLLogVerboseLevelError, @"GLCameraOutput::Error = %@", initializeSessionError);
            
            if (error != NULL)
            {
                *error = initializeSessionError;
            }
        }
    }
    return self;
}

- (void)dealloc
{
    [self tearDownSession];
    
    if (self.rotateVideoOrientationToStatusBarOrientation)
    {
        self.rotateVideoOrientationToStatusBarOrientation = NO;
    }
    
    [self.context lock:^{
        
        [self cleanUpTextures];
        [self.vertexArray cleanup];
        [self.fbo cleanup];
    }];
}

- (void)applicationDidChangeStatusBarOrientation:(id)sender
{
    self.videoOrientation = GLCameraOutputAVCaptureVideoOrientationFromUIInterfaceOrientation([UIApplication sharedApplication].statusBarOrientation);
}

#pragma mark -
#pragma mark Setters

- (void)setSize:(CGSize)size
{
    if (!CGSizeEqualToSize(size, self.size) && !CGSizeEqualToSize(size, CGSizeZero))
    {
        _size = size;
        
        [self.context lock];
        [self.fbo cleanup];
        [self setupVAO];
        
        self.fbo = [GLFrameBuffer frameBufferWithFormat:GLFrameBufferFormatRGB565 size:size context:self.context];
        
        [self.context unlock];
    }
}

- (void)setScale:(CGFloat)scale
{
    _scale = MAX(MIN(scale, 2.0f), 1.0f); // clampf
}

- (void)setVideoOrientation:(AVCaptureVideoOrientation)videoOrientation
{
    _videoOrientation = videoOrientation;
    
    for (AVCaptureOutput* output in self.session.outputs)
    {
        AVCaptureConnection* captureConnection = [output connectionWithMediaType:AVMediaTypeVideo];
        if (![output isKindOfClass:[AVCaptureVideoDataOutput class]] && captureConnection.isVideoOrientationSupported)
        {
            captureConnection.videoOrientation = self.videoOrientation;
        }
    }
    
    [self.context lock:^{ [self setupVAO]; }];
}

- (void)setRotateVideoOrientationToStatusBarOrientation:(BOOL)rotateVideoOrientationToStatusBarOrientation
{
    if (_rotateVideoOrientationToStatusBarOrientation != rotateVideoOrientationToStatusBarOrientation)
    {
        _rotateVideoOrientationToStatusBarOrientation = rotateVideoOrientationToStatusBarOrientation;
        
        if (_rotateVideoOrientationToStatusBarOrientation)
        {
            self.videoOrientation = GLCameraOutputAVCaptureVideoOrientationFromUIInterfaceOrientation([UIApplication sharedApplication].statusBarOrientation);
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidChangeStatusBarOrientation:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
        }
    }
}

#pragma mark -

- (void)cleanUpTextures
{
    if (_lumaTexture)
    {
        CFRelease(_lumaTexture);
        _lumaTexture = NULL;
    }
    
    if (_chromaTexture)
    {
        CFRelease(_chromaTexture);
        _chromaTexture = NULL;
    }
    
    // Periodic texture cache flush every frame
    CVOpenGLESTextureCacheFlush(self.context.videoTextureCache, 0);
}

- (void)setupVAO
{
    CGFloat x = 0.0f, y = 0.0f;
    
    if (CGSizeEqualToSize(self.size, CGSizeZero) || CGSizeEqualToSize(self.outputSize, CGSizeZero))
    {
        return;
    }
    
    if (self.videoOrientation == AVCaptureVideoOrientationPortrait || self.videoOrientation == AVCaptureVideoOrientationPortraitUpsideDown)
    {
        CGRect rect = GLCameraOutputRectByCalculateAspectOfSizes(self.size, self.outputSize);
        x = (rect.size.width / self.size.width) - (rect.origin.x / self.size.width);
        y = (rect.size.height / self.size.height) - (rect.origin.y / self.size.height);
    }
    else
    {
        CGRect rect = GLCameraOutputRectByCalculateAspectOfSizes(self.size, CGSizeMake(self.outputSize.height, self.outputSize.width));
        x = (rect.size.width / self.size.width) - (rect.origin.x / self.size.width);
        y = (rect.size.height / self.size.height) - (rect.origin.y / self.size.height);
    }
    
    [self.vertexArray map:^(void *data) {
        
        GLfloat* p = (GLfloat *)data;
        
        if (self.videoOrientation == AVCaptureVideoOrientationPortrait)
        {
            p[0] = -x; p[1] = y;
            p[4] = x;  p[5] = y;
            p[8] = -x; p[9] = -y;
            p[12] = x; p[13] = -y;
        }
        else if (self.videoOrientation == AVCaptureVideoOrientationPortraitUpsideDown)
        {
            p[0] = x;  p[1] = -y;
            p[4] = -x; p[5] = -y;
            p[8] = x;  p[9] = y;
            p[12] = -x;p[13] = y;
        }
        else if ((self.videoOrientation == AVCaptureVideoOrientationLandscapeLeft && self.captureDevicePosition == AVCaptureDevicePositionBack) ||
                 (self.videoOrientation == AVCaptureVideoOrientationLandscapeRight && self.captureDevicePosition == AVCaptureDevicePositionFront))
        {
            p[0] = x;  p[1] = y;
            p[4] = x;  p[5] = -y;
            p[8] = -x; p[9] = y;
            p[12] = -x;p[13] = -y;
        }
        else if ((self.videoOrientation == AVCaptureVideoOrientationLandscapeRight && self.captureDevicePosition == AVCaptureDevicePositionBack) ||
                 (self.videoOrientation == AVCaptureVideoOrientationLandscapeLeft && self.captureDevicePosition == AVCaptureDevicePositionFront))
        {
            p[0] = -x; p[1] = -y;
            p[4] = -x; p[5] = y;
            p[8] = x;  p[9] = -y;
            p[12] = x; p[13] = y;
        }
        
        p[2] = 0.0f;   p[3] = 1.0f;
        p[6] = 0.0f;   p[7] = 0.0f;
        p[10] = 1.0f;  p[11] = 1.0f;
        p[14] = 1.0f;  p[15] = 0.0f;
    }];
}

#pragma mark -
#pragma mark Session

- (BOOL)initializeSession:(NSError **)error
{
    AVCaptureDevicePosition position = [GLCameraOutput defaultCaptureDevicePosition];
    
    if (position != AVCaptureDevicePositionUnspecified)
    {
        NSError* captureDeviceError = nil;
        self.captureDevice = [GLCameraOutput captureDeviceWithPosition:position];
        self.captureDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:self.captureDevice error:&captureDeviceError];
        
        if (self.captureDeviceInput)
        {
            self.session = [[AVCaptureSession alloc] init];
            self.session.sessionPreset = self.captureSessionPreset;
            
            if ([self.session canAddInput:self.captureDeviceInput])
            {
                [self.session addInput:self.captureDeviceInput];
                
                AVCaptureVideoDataOutput* output = [[AVCaptureVideoDataOutput alloc] init];
                output.alwaysDiscardsLateVideoFrames = NO;
                
                for (NSNumber* currentPixelFormat in output.availableVideoCVPixelFormatTypes)
                {
                    if ([currentPixelFormat intValue] == kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)
                    {
                        _supportsFullYUVRange = YES;
                    }
                }
                
                if (_supportsFullYUVRange)
                {
                    output.videoSettings = @{(id)kCVPixelBufferPixelFormatTypeKey:@(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)};
                }
                else
                {
                    output.videoSettings = @{(id)kCVPixelBufferPixelFormatTypeKey:@(kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange)};
                }
                
                // For iPhone 4 needs use main queue
                if ([GLCameraOutputPlatformString() rangeOfString:@"iPhone3,"].location != NSNotFound)
                {
                    [output setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
                }
                else
                {
                    _camera_output_queue = dispatch_queue_create("com.splitpic.camera_output_queue", NULL);
                    [output setSampleBufferDelegate:self queue:_camera_output_queue];
                }
                
                [self.session addOutput:output];
                return YES;
            }
            else
            {
                self.session = nil;
                
                if (error != NULL)
                {
                    *error = [NSError errorWithDomain:@"GLCameraOutput" code:GLCameraOutputCantAddInputDeviceErrorCode userInfo:@{NSLocalizedDescriptionKey:@"Can't add device input"}];
                }
                return NO;
            }
        }
        else
        {
            if (error != NULL)
            {
                *error = captureDeviceError;
            }
            return NO;
        }
    }
    else
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:@"GLCameraOutput" code:GLCameraOutputFailedInitializeAVCaptureSessionErrorCode userInfo:@{NSLocalizedDescriptionKey:@"Failed initialize AVCaptureSession"}];
        }
        return NO;
    }
}

- (void)tearDownSession
{
    [self stopRunning];
    
    [self.session.inputs.copy enumerateObjectsUsingBlock:^(AVCaptureInput* input, NSUInteger index, BOOL *stop) {
        
        [self.session removeInput:input];
    }];
    
    [self.session.outputs.copy enumerateObjectsUsingBlock:^(AVCaptureOutput* output, NSUInteger index, BOOL *stop) {
        
        [self.session removeOutput:output];
    }];
    
    self.session = nil;
}

#pragma mark -

- (void)startRunning
{
    if (self.session && !self.session.isRunning)
    {
        [self.session startRunning];
        
        _running = self.session.isRunning;
        _paused = NO;
    }
}

- (void)stopRunning
{
    if (self.session && self.session.isRunning)
    {
        _running = NO;
        
        [self.session stopRunning];
        [self cleanUpTextures];
    }
}

- (void)pause
{
    _paused = YES;
}

- (void)resume
{
    _paused = NO;
}

#pragma mark -
#pragma mark Camera

- (AVCaptureDevicePosition)flipCamera
{
    if ([GLCameraOutput canFlipCamera])
    {
        BOOL pause = _paused;
        if (!pause)
        {
            [self pause];
        }
        
        AVCaptureDevicePosition position = self.captureDevice.position == AVCaptureDevicePositionBack ? AVCaptureDevicePositionFront : AVCaptureDevicePositionBack;
        AVCaptureDevice* captureDevice = [GLCameraOutput captureDeviceWithPosition:position];
        AVCaptureDeviceInput* captureDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:nil];
        
        [self.session beginConfiguration];
        [self.session removeInput:self.captureDeviceInput];
        
        if (![captureDevice supportsAVCaptureSessionPreset:self.captureSessionPreset])
        {
            self.session.sessionPreset = AVCaptureSessionPresetPhoto;
        }
        else if (![self.session.sessionPreset isEqualToString:self.captureSessionPreset])
        {
            self.session.sessionPreset = self.captureSessionPreset;
        }
        
        if ([self.session canAddInput:captureDeviceInput])
        {
            [self.session addInput:captureDeviceInput];
            self.captureDevice = captureDevice;
            self.captureDeviceInput = captureDeviceInput;
        }
        else
        {
            [self.session addInput:self.captureDeviceInput];
        }
        
        [self.session commitConfiguration];
        [self setVideoOrientation:self.videoOrientation];
        
        if (!pause)
        {
            [self resume];
        }
        
        return position;
    }
    else
    {
        return self.captureDevice.position;
    }
}

- (AVCaptureDevicePosition)captureDevicePosition
{
    return self.captureDevice.position;
}

- (BOOL)canSetFocus
{
    return [self.captureDevice isFocusPointOfInterestSupported] && [self.captureDevice isFocusModeSupported:AVCaptureFocusModeAutoFocus];
}

- (BOOL)setFocusAtPoint:(CGPoint)point
{
    if ([self canSetFocus])
    {
        if ([self.captureDevice lockForConfiguration:nil])
        {
            [self.captureDevice setFocusPointOfInterest:CGPointMake(point.x / self.scale + (self.scale - 1.0f) / (self.scale * 2.0), point.y / self.scale + (self.scale - 1.0f) / (self.scale * 2.0))];
            [self.captureDevice setFocusMode:AVCaptureFocusModeAutoFocus];
            [self.captureDevice unlockForConfiguration];
            return YES;
        }
    }
    return NO;
}

- (BOOL)canSetExposureWithCaptureExposureMode:(AVCaptureExposureMode)captureExposureMode
{
    return self.captureDevice.isExposurePointOfInterestSupported && [self.captureDevice isExposureModeSupported:captureExposureMode];
}

- (BOOL)setExposureAtPoint:(CGPoint)point withCaptureExposureMode:(AVCaptureExposureMode)captureExposureMode
{
    if ([self canSetExposureWithCaptureExposureMode:captureExposureMode])
    {
        if ([self.captureDevice lockForConfiguration:nil])
        {
            [self.captureDevice setExposurePointOfInterest:CGPointMake(point.x / self.scale + (self.scale - 1.0f) / (self.scale * 2.0), point.y / self.scale + (self.scale - 1.0f) / (self.scale * 2.0))];
            [self.captureDevice setExposureMode:captureExposureMode];
            [self.captureDevice unlockForConfiguration];
            return YES;
        }
    }
    return NO;
}

#pragma mark -
#pragma mark Binding

- (void)bindTextureToGLProgram:(GLProgram *)program uniform:(GLuint)uniform textureUnit:(GLenum)unit
{
    glActiveTexture(unit);
    glBindTexture(GL_TEXTURE_2D, self.fbo.texHandle);
    [program setUniform1i:uniform value:unit - GL_TEXTURE0];
}

#pragma mark -
#pragma mark Draw

- (void)drawImage
{
    [self.fbo bind];
    
    [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
        
        [cache useGLProgram:self.cameraOutputProgram];
        [cache setViewportSize:self.size];
    }];
    
    [self.cameraOutputProgram setUniform1f:self.cameraOutputProgram.uniformScale value:self.scale];
    [self.cameraOutputProgram setUniform1i:self.cameraOutputProgram.uniformSamplerY value:0];
    [self.cameraOutputProgram setUniform1i:self.cameraOutputProgram.uniformSamplerUV value:1];
    
    [self.vertexArray bind];
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    [self.vertexArray unbind];
    
    [self.fbo unbind];
}

#pragma mark -
#pragma mark AVCaptureVideoDataOutputSampleBufferDelegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    if (self.session == nil || _paused || CGSizeEqualToSize(self.size, CGSizeZero) || [GLCameraOutput authorizationStatus] != AVAuthorizationStatusAuthorized)
    {
        return;
    }
    
    if (!self.context.videoTextureCache)
    {
        GLLog(GLLogVerboseLevelError, @"No video texture cache");
        return;
    }
    
    [self.context lock];
    
    CVReturn err;
    CVImageBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    size_t width = CVPixelBufferGetWidth(pixelBuffer);
    size_t height = CVPixelBufferGetHeight(pixelBuffer);
    
    if (self.outputSize.width != height || self.outputSize.height != width)
    {
        self.outputSize = CGSizeMake(height, width);
        GLLog(GLLogVerboseLevelWarning, @"New outputSize = %@", NSStringFromCGSize(self.outputSize));
        [self setupVAO];
    }
    
    [self cleanUpTextures];
    
    // CVOpenGLESTextureCacheCreateTextureFromImage will create GLES texture
    // optimally from CVImageBufferRef.
    
    // Y-plane
    glActiveTexture(GL_TEXTURE0);
    err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                       self.context.videoTextureCache,
                                                       pixelBuffer,
                                                       NULL,
                                                       GL_TEXTURE_2D,
                                                       GL_LUMINANCE,
                                                       (GLsizei)width,
                                                       (GLsizei)height,
                                                       GL_LUMINANCE,
                                                       GL_UNSIGNED_BYTE,
                                                       0,
                                                       &_lumaTexture);
    if (err)
    {
        GLLog(GLLogVerboseLevelError, @"Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
    }
    
    glBindTexture(CVOpenGLESTextureGetTarget(_lumaTexture), CVOpenGLESTextureGetName(_lumaTexture));
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    // UV-plane
    glActiveTexture(GL_TEXTURE1);
    err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                       self.context.videoTextureCache,
                                                       pixelBuffer,
                                                       NULL,
                                                       GL_TEXTURE_2D,
                                                       GL_LUMINANCE_ALPHA,
                                                       (GLsizei)width / 2,
                                                       (GLsizei)height / 2,
                                                       GL_LUMINANCE_ALPHA,
                                                       GL_UNSIGNED_BYTE,
                                                       1,
                                                       &_chromaTexture);
    if (err)
    {
        GLLog(GLLogVerboseLevelError, @"Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
    }
    
    glBindTexture(CVOpenGLESTextureGetTarget(_chromaTexture), CVOpenGLESTextureGetName(_chromaTexture));
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    [self drawImage];
    
    [self.context unlock];
}

#pragma mark -
#pragma mark Debug

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
- (id)debugQuickLookObject
{
    return [self.fbo performSelector:@selector(debugQuickLookObject)];
}
#pragma clang diagnostic pop

@end
