//
//  GLStillCamera.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 3/26/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLCameraOutput.h"

extern NSInteger const GLStillCameraCaptureConnectionNotFoundErrorCode;
extern NSInteger const GLStillCameraCaptureCantCameraAccessErrorCode;
extern NSInteger const GLStillCameraCaptureStillImageOutputNotFoundErrorCode;
extern NSInteger const GLStillCameraCaptureBeingCapturedErrorCode;

@interface GLStillCamera : GLCameraOutput

@property (nonatomic) BOOL flashAvailable;
@property (nonatomic) AVCaptureFlashMode flashMode;

- (void)captureImageWithBlock:(void(^)(NSData* imageData, NSError* error))block;

@end
