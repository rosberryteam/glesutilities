//
//  GLStillCamera.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 3/26/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLStillCamera.h"
#import "GLCameraOutput+Private.h"

NSInteger const GLStillCameraCaptureConnectionNotFoundErrorCode = 401;
NSInteger const GLStillCameraCaptureCantCameraAccessErrorCode = 402;
NSInteger const GLStillCameraCaptureStillImageOutputNotFoundErrorCode = 403;
NSInteger const GLStillCameraCaptureBeingCapturedErrorCode = 404;

@interface GLStillCamera ()

@property (nonatomic) AVCaptureStillImageOutput* stillImageOutput;

@end

@implementation GLStillCamera

- (BOOL)initializeSession:(NSError **)error
{
    if ([super initializeSession:error])
    {
        AVCaptureStillImageOutput* stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
        stillImageOutput.outputSettings = @{AVVideoCodecKey:AVVideoCodecJPEG};
        
        if ([self.session canAddOutput:stillImageOutput])
        {
            self.stillImageOutput = stillImageOutput;
            [self.session addOutput:self.stillImageOutput];
            
            return YES;
        }
        else
        {
            if (error != NULL)
            {
                *error = [NSError errorWithDomain:@"GLStillCameraDomain" code:400 userInfo:@{NSLocalizedDescriptionKey:@"Can't add still image output"}];
            }
        }
    }
    return NO;
}

- (void)tearDownSession
{
    if (self.stillImageOutput)
    {
        [self.session removeOutput:self.stillImageOutput];
    }
    
    [super tearDownSession];
}

- (AVCaptureConnection*)captureConnection
{
    for (AVCaptureConnection* connection in self.stillImageOutput.connections)
    {
        for (AVCaptureInputPort* port in [connection inputPorts])
        {
            if ([[port mediaType] isEqual:AVMediaTypeVideo])
            {
                return connection;
            }
        }
    }
    return nil;
}

- (BOOL)flashAvailable
{
    return self.captureDevice.flashAvailable;
}

- (void)setFlashMode:(AVCaptureFlashMode)flashMode
{
    if ([self.captureDevice isFlashModeSupported:flashMode] && [self.captureDevice lockForConfiguration:nil])
    {
        self.captureDevice.flashMode = flashMode;
        [self.captureDevice unlockForConfiguration];
    }
}

- (AVCaptureFlashMode)flashMode
{
    return self.captureDevice.flashMode;
}

- (void)captureImageWithBlock:(void(^)(NSData* imageData, NSError* error))block
{
    if ([GLCameraOutput authorizationStatus] != AVAuthorizationStatusAuthorized)
    {
        block(nil, [NSError errorWithDomain:@"GLCameraOutputDomain" code:GLStillCameraCaptureCantCameraAccessErrorCode userInfo:@{NSLocalizedDescriptionKey:@"Can't access to device camera"}]);
        return;
    }
    
    AVCaptureConnection* captureConnection = [self captureConnection];
    if (!captureConnection)
    {
        block(nil, [NSError errorWithDomain:@"GLCameraOutputDomain" code:GLStillCameraCaptureConnectionNotFoundErrorCode userInfo:@{NSLocalizedDescriptionKey:@"Capture Connection not found."}]);
        return;
    }
    
    if (!self.stillImageOutput)
    {
        block(nil, [NSError errorWithDomain:@"GLCameraOutputDomain" code:GLStillCameraCaptureStillImageOutputNotFoundErrorCode userInfo:@{NSLocalizedDescriptionKey:@"Still Image Output not found."}]);
        return;
    }
    
    if (self.stillImageOutput.capturingStillImage)
    {
        block(nil, [NSError errorWithDomain:@"GLCameraOutputDomain" code:GLStillCameraCaptureBeingCapturedErrorCode userInfo:@{NSLocalizedDescriptionKey:@"Device is being captured."}]);
        return;
    }
    
    @try {
        
        [self.stillImageOutput captureStillImageAsynchronouslyFromConnection:captureConnection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
           
            if (error)
            {
                block(nil, error);
            }
            else
            {
                NSData* data = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                block(data, nil);
            }
        }];
    }
    @catch (NSException *exception) {
        
        block(nil, [NSError errorWithDomain:@"GLCameraOutputDomain" code:400 userInfo:@{NSLocalizedDescriptionKey:exception.reason}]);
    }
}

@end
