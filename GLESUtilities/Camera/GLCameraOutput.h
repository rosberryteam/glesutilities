//
//  GLCameraOutput.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 3/26/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>
#import "GLContext.h"

extern NSInteger const GLCameraOutputCantAddInputDeviceErrorCode;
extern NSInteger const GLCameraOutputFailedInitializeAVCaptureSessionErrorCode;

#ifdef __cplusplus
extern "C" {
#endif
    
    AVCaptureVideoOrientation GLCameraOutputAVCaptureVideoOrientationFromUIInterfaceOrientation(UIInterfaceOrientation orientation);
    
#ifdef __cplusplus
}
#endif

@class GLProgram;

/**
 * GLCameraOutput use GLContext lock/unlock when it is necessary
 */

@interface GLCameraOutput : NSObject <GLContext, AVCaptureVideoDataOutputSampleBufferDelegate>

@property (nonatomic,readonly,getter=isRunning) BOOL running;
@property (nonatomic,readonly,getter=isPaused) BOOL paused;
@property (nonatomic) CGFloat scale;
@property (nonatomic) CGSize size;
@property (nonatomic) AVCaptureVideoOrientation videoOrientation;
@property (nonatomic) BOOL rotateVideoOrientationToStatusBarOrientation;

+ (AVAuthorizationStatus)authorizationStatus;
+ (void)requestAccessWithCompletionHandler:(void (^)(BOOL granted))handler;

+ (BOOL)backCameraAvailable;
+ (BOOL)frontCameraAvailable;
+ (BOOL)canFlipCamera;
+ (AVCaptureDevicePosition)defaultCaptureDevicePosition;

+ (instancetype)cameraWithSize:(CGSize)size context:(GLContext *)context error:(NSError **)error;
+ (instancetype)cameraWithSize:(CGSize)size context:(GLContext *)context captureSessionPreset:(NSString *)captureSessionPreset error:(NSError **)error;

- (instancetype)initWithSize:(CGSize)size context:(GLContext *)context captureSessionPreset:(NSString *)captureSessionPreset error:(NSError **)error;

- (void)startRunning;
- (void)stopRunning;

- (void)pause;
- (void)resume;

- (AVCaptureDevicePosition)flipCamera;
- (AVCaptureDevicePosition)captureDevicePosition;

- (BOOL)canSetFocus;

/**
 Set camera focus to specific point.
 
 @param point Should be from {0,0} (top-left) to {1,1} (bottom-right).
 */

- (BOOL)setFocusAtPoint:(CGPoint)point;

- (BOOL)canSetExposureWithCaptureExposureMode:(AVCaptureExposureMode)captureExposureMode;

/**
 Set camera exposure to specific point.
 
 @param point Should be from {0,0} (top-left) to {1,1} (bottom-right).
 */

- (BOOL)setExposureAtPoint:(CGPoint)point withCaptureExposureMode:(AVCaptureExposureMode)captureExposureMode;


- (void)bindTextureToGLProgram:(GLProgram *)program uniform:(GLuint)uniform textureUnit:(GLenum)unit;

@end
