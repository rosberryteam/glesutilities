//
//  GLCameraOutput+Private.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 7/30/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLCameraOutput.h"

@class GLCameraOutputProgram;
@class GLFrameBuffer;
@class GLVertexArray;

@interface GLCameraOutput (Private)

@property (nonatomic) GLCameraOutputProgram* cameraOutputProgram;
@property (nonatomic) GLFrameBuffer* fbo;
@property (nonatomic) GLVertexArray* vertexArray;

@property (nonatomic) AVCaptureSession* session;
@property (nonatomic) AVCaptureDevice* captureDevice;
@property (nonatomic) AVCaptureDeviceInput* captureDeviceInput;
@property (nonatomic) NSString* captureSessionPreset;
@property (nonatomic) BOOL supportsFullYUVRange;
@property (nonatomic) CGSize outputSize;

- (BOOL)initializeSession:(NSError **)error;
- (void)tearDownSession;
- (void)drawImage;

@end
