//
//  GLCamera+SimulatorSupport.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 3/26/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLCameraOutput.h"

@interface GLCameraOutput (SimulatorSupport)

@end
