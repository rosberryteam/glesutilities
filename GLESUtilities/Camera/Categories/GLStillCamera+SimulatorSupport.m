//
//  GLStillCamera+SimulatorSupport.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 4/1/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLStillCamera+SimulatorSupport.h"

#import <objc/runtime.h>

#if TARGET_IPHONE_SIMULATOR
#import "GLStillCamera+SimulatorSupport_RawImageData.h"
#endif

@implementation GLStillCamera (SimulatorSupport)

#if TARGET_IPHONE_SIMULATOR

+ (void)exchangeInstanceMethod:(SEL)originMethod new:(SEL)newMethod
{
    Method _origMethod = class_getInstanceMethod([self class], originMethod);
    Method _newMethod = class_getInstanceMethod([self class], newMethod);
    method_exchangeImplementations(_origMethod, _newMethod);
}

+ (void)load
{
    /* Exchange -captureImageWithBlock: */
    [self exchangeInstanceMethod:@selector(captureImageWithBlock:) new:@selector(_captureImageWithBlock:)];
}

- (void)_captureImageWithBlock:(void(^)(NSData* imageData, NSError* error))block
{
    NSData* data = [NSData dataWithBytes:GLStillCamera_SimulatorSupport_RawImageData length:GLStillCamera_SimulatorSupport_RawImageData_Lenght];
    block(data, nil);
}

#endif

@end
