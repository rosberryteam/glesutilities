//
//  GLCamera+SimulatorSupport.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 3/26/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <GLCameraOutput+SimulatorSupport.h>
#import "GLCameraOutput+Private.h"

#import <objc/runtime.h>

// TODO: need draw image (exchange -drawImage and call in it -setScale: / -setSize:)

#if TARGET_IPHONE_SIMULATOR

@implementation GLCameraOutput (SimulatorSupport)

+ (void)exchangeInstanceMethod:(SEL)originMethod new:(SEL)newMethod
{
    Method _origMethod = class_getInstanceMethod([self class], originMethod);
    Method _newMethod = class_getInstanceMethod([self class], newMethod);
    method_exchangeImplementations(_origMethod, _newMethod);
}

+ (void)load
{
    /* Exchange -initializeSession */
    [self exchangeInstanceMethod:@selector(initializeSession:) new:@selector(_initializeSession:)];
    
    /* Exchange -startRunning */
    [self exchangeInstanceMethod:@selector(startRunning) new:@selector(_startRunning)];
    
    /* Exchange -stopRunning */
    [self exchangeInstanceMethod:@selector(stopRunning) new:@selector(_stopRunning)];
    
    /* Exchange -isRunning */
    [self exchangeInstanceMethod:@selector(isRunning) new:@selector(_isRunning)];
    
    /* Exchange -setSize: */
    [self exchangeInstanceMethod:@selector(setSize:) new:@selector(_setSize:)];
    
    /* Exchange -canSetExposureWithCaptureExposureMode: */
    [self exchangeInstanceMethod:@selector(canSetExposureWithCaptureExposureMode:) new:@selector(_canSetExposureWithCaptureExposureMode:)];
}

- (BOOL)_initializeSession:(NSError **)error
{
    return YES;
}

- (void)_startRunning
{
    self.GLCameraOutputSimulatorSupport_isRunning = YES;
}

- (void)_stopRunning
{
    self.GLCameraOutputSimulatorSupport_isRunning = NO;
}

- (BOOL)_isRunning
{
    return self.GLCameraOutputSimulatorSupport_isRunning;
}

- (void)_setSize:(CGSize)size
{
    BOOL needsDraw = !CGSizeEqualToSize(size, self.size);
    
    [self _setSize:size];
    
    if (needsDraw)
    {
        [self drawImage];
    }
}

- (BOOL)_canSetExposureWithCaptureExposureMode:(AVCaptureExposureMode)captureExposureMode
{
    return YES;
}

#pragma mark -
#pragma mark is Runnig Setter/Getter

- (void)setGLCameraOutputSimulatorSupport_isRunning:(BOOL)GLCameraOutputSimulatorSupport_isRunning
{
    objc_setAssociatedObject(self, @selector(GLCameraOutputSimulatorSupport_isRunning), @(GLCameraOutputSimulatorSupport_isRunning), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)GLCameraOutputSimulatorSupport_isRunning
{
    return [objc_getAssociatedObject(self, @selector(GLCameraOutputSimulatorSupport_isRunning)) boolValue];
}

@end

#endif
