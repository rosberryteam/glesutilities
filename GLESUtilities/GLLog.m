//
//  GLLog.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 11/12/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLLog.h"

static GLLogVerboseLevel GLLogVerboseLevelValue = GLLogVerboseLevelError;

void GLLogSetVerboseLevel(GLLogVerboseLevel level)
{
    GLLogVerboseLevelValue = level;
}

void GLLog(GLLogVerboseLevel level, NSString *format, ...)
{
    if (level <= GLLogVerboseLevelValue)
    {
        va_list args;
        va_start(args, format);
        NSLogv(format, args);
        va_end(args);
    }
}
