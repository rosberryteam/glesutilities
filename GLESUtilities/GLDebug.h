//
//  GLDebug.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 4/10/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//


#define DEBUG_OPENGL_CALL 1

#if DEBUG_OPENGL_CALL

// https://www.opengl.org/wiki/OpenGL_Error
//
// GL_INVALID_ENUM, 0x0500
// Given when an enumeration parameter is not a legal enumeration for that function. This is given only for local problems;
// if the spec allows the enumeration in certain circumstances, where other parameters or state dictate those circumstances, then GL_INVALID_OPERATION is the result instead.
//
// GL_INVALID_VALUE, 0x0501
// Given when a value parameter is not a legal value for that function. This is only given for local problems; if the spec allows the value in certain circumstances,
// where other parameters or state dictate those circumstances, then GL_INVALID_OPERATION is the result instead.
//
// GL_INVALID_OPERATION, 0x0502
// Given when the set of state for a command is not legal for the parameters given to that command.
// It is also given for commands where combinations of parameters define what the legal parameters are.
//
// GL_OUT_OF_MEMORY, 0x0505
// Given when performing an operation that can allocate memory, and the memory cannot be allocated. The results of OpenGL functions that return this error are undefined;
// it is allowable for partial operations to happen.
//
// GL_INVALID_FRAMEBUFFER_OPERATION, 0x0506
// Given when doing anything that would attempt to read from or write/render to a framebuffer that is not complete.

#if DEBUG
#define GLDebugCheckGLError(...) \
{ \
    GLenum err = glGetError(); \
    if (err != GL_NO_ERROR) \
    { \
    int knownError = 1; \
    const char* error = ""; \
    switch(err) \
    { \
        case GL_INVALID_ENUM: \
            error = "GL_INVALID_ENUM"; \
            break; \
        case GL_INVALID_VALUE: \
            error = "GL_INVALID_VALUE"; \
            break; \
        case GL_INVALID_OPERATION: \
            error = "GL_INVALID_OPERATION"; \
            break; \
        case GL_OUT_OF_MEMORY: \
            error = "GL_OUT_OF_MEMORY"; \
            break; \
        case GL_INVALID_FRAMEBUFFER_OPERATION: \
            error = "GL_INVALID_FRAMEBUFFER_OPERATION"; \
            break; \
        default: \
            knownError = 0; \
        } \
        if (knownError) \
            printf("GLError: %s, %s at %s(%d)\n", #__VA_ARGS__, error, __FILE__, __LINE__); \
        else \
            printf("GLError: %s, 0x%04X at %s(%d)\n", #__VA_ARGS__, err, __FILE__, __LINE__); \
    } \
}
#else
#define GLDebugCheckGLError(...)
#endif


#define glCreateProgram()                       glCreateProgram();                      GLDebugCheckGLError(glCreateProgram);
#define glCreateShader(a)                       glCreateShader(a);                      GLDebugCheckGLError(glCreateShader);
#define glAttachShader(a, b)                    glAttachShader(a, b);                   GLDebugCheckGLError(glAttachShader);
#define glShaderSource(a, b, c, d)              glShaderSource(a, b, c, d);             GLDebugCheckGLError(glShaderSource);
#define glCompileShader(a)                      glCompileShader(a);                     GLDebugCheckGLError(glCompileShader);
#define glGetShaderiv(a, b, c)                  glGetShaderiv(a, b, c);                 GLDebugCheckGLError(glGetShaderiv);
#define glGetShaderInfoLog(a, b, c, d)          glGetShaderInfoLog(a, b, c, d);         GLDebugCheckGLError(glGetShaderInfoLog);
#define glBindAttribLocation(a, b, c)           glBindAttribLocation(a, b, c);          GLDebugCheckGLError(glBindAttribLocation);
#define glGetUniformLocation(a, b)              glGetUniformLocation(a, b);             GLDebugCheckGLError(glGetUniformLocation);
#define glLinkProgram(a)                        glLinkProgram(a);                       GLDebugCheckGLError(glLinkProgram);
#define glGetProgramiv(a, b, c)                 glGetProgramiv(a, b, c);                GLDebugCheckGLError(glGetProgramiv);
#define glDeleteShader(a)                       glDeleteShader(a);                      GLDebugCheckGLError(glDeleteShader);
#define glUseProgram(a)                         glUseProgram(a);                        GLDebugCheckGLError(glUseProgram);
#define glValidateProgram(a)                    glValidateProgram(a);                   GLDebugCheckGLError(glValidateProgram);
#define glGetProgramInfoLog(a, b, c, d)         glGetProgramInfoLog(a, b, c, d);        GLDebugCheckGLError(glGetProgramInfoLog);
#define glDeleteProgram(a)                      glDeleteProgram(a);                     GLDebugCheckGLError(glDeleteProgram);


#define glUniform1i(a, b)                       glUniform1i(a, b);                      GLDebugCheckGLError(glUniform1i);
#define glUniform1f(a, b)                       glUniform1f(a, b);                      GLDebugCheckGLError(glUniform1f);
#define glUniform2f(a,b,c)                      glUniform2f(a,b,c);                     GLDebugCheckGLError(glUniform2f);
#define glUniformMatrix4fv(a, b, c, d)          glUniformMatrix4fv(a, b, c, d);         GLDebugCheckGLError(glUniformMatrix4fv);


#define glGenBuffers(a,b)                       glGenBuffers(a,b);                      GLDebugCheckGLError(glGenBuffers);
#define glDeleteBuffers(a, b)                   glDeleteBuffers(a, b);                  GLDebugCheckGLError(glDeleteBuffers);
#define glBindBuffer(a,b)                       glBindBuffer(a,b);                      GLDebugCheckGLError(glBindBuffer);
#define glBufferData(a, b, c, d)                glBufferData(a, b, c, d);               GLDebugCheckGLError(glBufferData);
#define glMapBufferOES(a, b)                    glMapBufferOES(a, b);                   GLDebugCheckGLError(glMapBufferOES);
#define glUnmapBufferOES(a)                     glUnmapBufferOES(a);                    GLDebugCheckGLError(glUnmapBufferOES);


#define glGenVertexArraysOES(a, b)              glGenVertexArraysOES(a, b);             GLDebugCheckGLError(glGenVertexArraysOES);
#define glDeleteVertexArraysOES(a, b)           glDeleteVertexArraysOES(a, b);          GLDebugCheckGLError(glDeleteVertexArraysOES);
#define glBindVertexArrayOES(a)                 glBindVertexArrayOES(a);                GLDebugCheckGLError(glBindVertexArrayOES);


#define glGenFramebuffers(a, b)                 glGenFramebuffers(a, b);                GLDebugCheckGLError(glGenFramebuffers);
#define glBindFramebuffer(a, b)                 glBindFramebuffer(a, b);                GLDebugCheckGLError(glBindFramebuffer);
#define glDeleteFramebuffers(a, b)              glDeleteFramebuffers(a, b);             GLDebugCheckGLError(glDeleteFramebuffers);
#define glFramebufferTexture2D(a, b, c, d, e)   glFramebufferTexture2D(a, b, c, d, e);  GLDebugCheckGLError(glFramebufferTexture2D);
//#define glCheckFramebufferStatus(a)             glCheckFramebufferStatus(a); GLDebugCheckGLError(glCheckFramebufferStatus);

#define glGenRenderbuffers(a, b)                glGenRenderbuffers(a, b);               GLDebugCheckGLError(glGenRenderbuffers);
#define glBindRenderbuffer(a, b)                glBindRenderbuffer(a, b);               GLDebugCheckGLError(glBindRenderbuffer);
#define glGetRenderbufferParameteriv(a, b, c)   glGetRenderbufferParameteriv(a, b, c);  GLDebugCheckGLError(glGetRenderbufferParameteriv);
#define glFramebufferRenderbuffer(a, b, c, d)   glFramebufferRenderbuffer(a, b, c, d);  GLDebugCheckGLError(glFramebufferRenderbuffer);
#define glDeleteRenderbuffers(a, b)             glDeleteRenderbuffers(a, b);            GLDebugCheckGLError(glDeleteRenderbuffers);


#define glPixelStorei(a, b)                     glPixelStorei(a, b);                    GLDebugCheckGLError(glPixelStorei);
#define glGenTextures(a, b)                     glGenTextures(a, b);                    GLDebugCheckGLError(glGenTextures);
#define glActiveTexture(a)                      glActiveTexture(a);                     GLDebugCheckGLError(glActiveTexture);
#define glBindTexture(a, b)                     glBindTexture(a, b);                    GLDebugCheckGLError(glBindTexture);
#define glDeleteTextures(a, b)                  glDeleteTextures(a, b);                 GLDebugCheckGLError(glDeleteTextures);
#define glTexParameterf(a, b, c)                glTexParameterf(a, b, c);               GLDebugCheckGLError(glTexParameterf);
#define glTexParameteri(a, b, c)                glTexParameteri(a, b, c);               GLDebugCheckGLError(glTexParameteri);
#define glTexImage2D(a, b, c, d, e, f, g, i, k) glTexImage2D(a, b, c, d, e, f, g, i, k); GLDebugCheckGLError(glTexImage2D);


#define glEnableVertexAttribArray(a)            glEnableVertexAttribArray(a);           GLDebugCheckGLError(glEnableVertexAttribArray);
#define glVertexAttribPointer(a, b, c, d, e, f) glVertexAttribPointer(a, b, c, d, e, f); GLDebugCheckGLError(glVertexAttribPointer);

#define glDrawArrays(a, b, c)                   glDrawArrays(a, b, c);                  GLDebugCheckGLError(glDrawArrays);

#define glGetIntegerv(a, b)                     glGetIntegerv(a, b);                    GLDebugCheckGLError(glGetIntegerv);
#define glReadPixels(a, b, c, d, e, f, g)       glReadPixels(a, b, c, d, e, f, g);      GLDebugCheckGLError(glReadPixels);

#define glViewport(a, b, c, d)                  glViewport(a, b, c, d);                 GLDebugCheckGLError(glViewport);
#define glClearColor(a, b, c, d)                glClearColor(a, b, c, d);               GLDebugCheckGLError(glClearColor);
#define glClear(a)                              glClear(a);                             GLDebugCheckGLError(glClear);


#define glEnable(a)                             glEnable(a);                            GLDebugCheckGLError(glEnable);
#define glBlendFunc(a, b)                       glBlendFunc(a, b);                      GLDebugCheckGLError(glBlendFunc);
#define glBlendFuncSeparate(a, b, c, d)         glBlendFuncSeparate(a, b, c, d);        GLDebugCheckGLError(glBlendFuncSeparate);

#endif // #if DEBUG_OPENGL_CALL
