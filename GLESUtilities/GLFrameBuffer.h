//
//  GLFrameBuffer.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 3/25/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import "GLCleanupProtocol.h"
#import "GLCommon.h"
#import "GLContext.h"

@class GLTexture;
@class GLProgram;
@class UIImage;

typedef NS_ENUM(NSUInteger, GLFrameBufferFormat)
{
    GLFrameBufferFormatRGBA8888 = 0,
    GLFrameBufferFormatRGB565,
    GLFrameBufferFormatR8,
    GLFrameBufferFormatGLTexture
};

@interface GLFrameBuffer : NSObject <GLContext, GLCleanupProtocol>

@property (nonatomic,readonly) GLFrameBufferFormat format;
@property (nonatomic,readonly) GLuint handle;
@property (nonatomic,readonly) GLuint texHandle;
@property (nonatomic,readonly) CGSize size;

+ (instancetype)frameBufferWithFormat:(GLFrameBufferFormat)format size:(CGSize)size context:(GLContext *)context;
+ (instancetype)frameBufferWithGLTexture:(GLTexture *)texture context:(GLContext *)context;

- (void)bind;
- (void)unbind;

- (void)bindTextureToGLProgram:(GLProgram *)program uniform:(GLuint)uniform textureUnit:(GLenum)unit;

- (UIImage *)snapshot;

@end
