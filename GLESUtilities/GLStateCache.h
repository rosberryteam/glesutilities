//
//  GLStateCache.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 4/8/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLCommon.h"

#define USE_STATE_CACHE 1
#define PROFILE_STATE_CACHE 0

@class GLContext;
@class GLProgram;

@protocol GLStateCache <NSObject>
@required

- (void)reset;

- (void)setViewportSize:(CGSize)size;
- (void)setClearColorRed:(CGFloat)red green:(GLfloat)green blue:(GLfloat)blue alpha:(GLfloat)alpha;

- (void)useGLProgram:(GLProgram *)program;

- (void)bindFramebuffer:(GLuint)target framebuffer:(GLuint)framebuffer;
- (void)bindRenderbuffer:(GLuint)target framebuffer:(GLuint)renderbuffer;

@end

@interface GLStateCache : NSObject

+ (void)beginWithContext:(GLContext *)context block:(void(^)(id<GLStateCache> cache))block;
+ (void)contextDidDestroy:(GLContext *)context;

#if PROFILE_STATE_CACHE
+ (void)incRedundantCall;
+ (void)showStatistics;
#endif

@end
