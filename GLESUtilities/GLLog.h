//
//  GLLog.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 11/12/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, GLLogVerboseLevel)
{
    GLLogVerboseLevelNothing = 0,
    GLLogVerboseLevelError,
    GLLogVerboseLevelWarning
};

#if __cplusplus
extern "C" {
#endif
    
    void GLLogSetVerboseLevel(GLLogVerboseLevel level);
    void GLLog(GLLogVerboseLevel level, NSString *format, ...);
    
#if __cplusplus
}
#endif
