//
//  GLVertexArray.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 5/8/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLVertexArray.h"
#import "GLDebug.h"

@interface GLVertexArray ()

@property (nonatomic) GLContext* context;

@end

@implementation GLVertexArray

+ (instancetype)vertexArrayWithDataSize:(NSUInteger)size context:(GLContext *)context
{
    return [[self alloc] initWithDataSize:size context:context];
}

- (instancetype)initWithDataSize:(NSUInteger)size context:(GLContext *)context
{
    self = [super init];
    if (self)
    {
        NSAssert(size != 0, @"Data size must be > 0");
        NSParameterAssert(context);
        
        _context = context;
        _dataSize = size;
        
        void* data = calloc(1, size);
        NSAssert(data, @"Failed allocate memory for size %zd", size);
        
        glGenVertexArraysOES(1, &_handle);
        glGenBuffers(1, &_vboHandle);
        
        NSAssert(_handle != 0 || _vboHandle != 0, @"Failed create VBO and VAO");
        
        glBindVertexArrayOES(_handle);
        glBindBuffer(GL_ARRAY_BUFFER, _vboHandle);
        glBufferData(GL_ARRAY_BUFFER, size, data, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArrayOES(0);
        
        free(data);
    }
    return self;
}

- (void)dealloc
{
    if (_handle || _vboHandle)
    {
        [self.context lock:^{ [self cleanup]; }];
    }
}

#pragma mark -
#pragma mark Bind/Unbind

- (void)bind
{
    glBindVertexArrayOES(_handle);
}

- (void)unbind
{
    glBindVertexArrayOES(0);
}

#pragma mark -
#pragma mark Setup Data

- (void)setupVertexAttrib:(GLuint)attrib size:(GLuint)size stride:(GLuint)stride offset:(GLuint)offset
{
    [self bind];
    
    glBindBuffer(GL_ARRAY_BUFFER, _vboHandle);
    glVertexAttribPointer(attrib, size, GL_FLOAT, GL_FALSE, stride, (void *)(NSUInteger)offset);
    glEnableVertexAttribArray(attrib);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    [self unbind];
}

- (void)setupVertexAttrib:(GLuint)attrib size:(GLuint)size
{
    [self setupVertexAttrib:attrib size:size stride:0 offset:0];
}

#pragma mark -
#pragma mark Map Data

- (BOOL)map:(void(^)(void* data))block
{
    BOOL result = NO;
    glBindBuffer(GL_ARRAY_BUFFER, _vboHandle);
    
    void* data = glMapBufferOES(GL_ARRAY_BUFFER, GL_WRITE_ONLY_OES);
    if (data)
    {
        block(data);
        glUnmapBufferOES(GL_ARRAY_BUFFER);
        result = YES;
    }
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return result;
}

#pragma mark -
#pragma mark GLCleanupProtocol

- (void)cleanup
{
    if (_handle)
    {
        glDeleteVertexArraysOES(1, &_handle);
        _handle = 0;
    }
    
    if (_vboHandle)
    {
        glDeleteBuffers(1, &_vboHandle);
        _vboHandle = 0;
    }
}

#pragma mark -

- (NSString *)description
{
    [self.context set];
    
    NSMutableString* string = [NSMutableString stringWithFormat:@"<%@: %p> vboHandle = %zd, vaoHandle = %zd, size = %zd, data:\n", [self class], self, self.vboHandle, self.handle, self.dataSize];
    
    void (^block)() = ^(void *data)
    {
        for (NSInteger i = 0; i < self.dataSize / sizeof(float); i += 2)
        {
            float* p = (float *)data + i;
            [string appendFormat:@"{%.2f, %.2f}\n", p[0], p[1]];
        }
    };
    
    if (![self map:block])
    {
        [string appendString:@"Failed to map buffer!"];
    }
    
    return [NSString stringWithString:string];
}

@end
