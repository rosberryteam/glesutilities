//
//  GLContext.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 03/05/2013.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <CoreVideo/CoreVideo.h>
#import <Foundation/Foundation.h>
#import <OpenGLES/EAGLDrawable.h>
#import "GLCommon.h"

@interface GLContext : NSObject

@property (nonatomic,readonly) CVOpenGLESTextureCacheRef videoTextureCache;

+ (GLContext *)sharedContext;
+ (GLContext *)context;
+ (GLContext *)contextWithSharedContext:(GLContext *)context;

+ (NSArray *)extensions;
+ (BOOL)supportExtension:(NSString *)extension;
+ (BOOL)supportExtensionTextureRG;
+ (BOOL)supportDiscardFramebuffer;

+ (GLint)maximumTextureSize;

+ (CGSize)sizeThatFitInMaxSupportSize:(CGSize)inputSize;
+ (CGFloat)scale;

- (void)set;

/**
 * Should use lock/unlock for all GL operations and for creation id<GLContext> objects;
 * id<GLContext> objects themselves use lock/unlock in -dealloc
 */

- (void)lock;
- (void)unlock;

/**
 * lock context, perform block and then unlock
 */

- (void)lock:(dispatch_block_t)block;

- (BOOL)renderbufferStorage:(NSUInteger)target fromDrawable:(id<EAGLDrawable>)drawable;
- (BOOL)presentRenderbuffer:(NSUInteger)target;

@end


@protocol GLContext <NSObject>
@required

@property (nonatomic,readonly) GLContext* context;

@end
