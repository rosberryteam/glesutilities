//
//  GLView.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 10/14/13.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLContext.h"

typedef NS_OPTIONS(NSInteger, GLViewOptions)
{
    GLViewOptionsNone = 1 << 0,
    GLViewOptionsDepthBuffer = 1 << 1,
    GLViewOptionsMSAA = 1 << 2,
};

@protocol GLViewDelegate;

/**
 * GLView use GLContext lock/unlock when it is necessary
 */

@interface GLView : UIView <GLContext>

@property (nonatomic,weak) IBOutlet id<GLViewDelegate> delegate;

@property (nonatomic,readonly) CGSize sizeInPixels;
@property (nonatomic) GLViewOptions options;
@property (nonatomic) GLContext* context;

- (BOOL)display;
- (void)bindDrawable;

- (UIImage *)snapshot;

@end

@protocol GLViewDelegate <NSObject>
@optional

- (void)glView:(GLView *)view didRecreateDisplayFramebuffer:(CGSize)sizeInPixels;

@required

- (void)glView:(GLView *)view drawInRect:(CGRect)rect;

@end
