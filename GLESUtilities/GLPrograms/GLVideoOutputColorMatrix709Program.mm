//
//  GLVideoOutputColorMatrix709Program.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 6/14/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLVideoOutputColorMatrix709Program.h"
#import "GLCommon.h"
#import "GLDebug.h"

NSString* GLVideoOutputColorMatrix709ProgramVertexShader = SHADER_STRING
(
 attribute vec4 position;
 attribute vec2 inputTextureCoordinate;
 
 varying vec2 textureCoordinate;
 
 void main()
 {
     gl_Position = position;
     textureCoordinate = inputTextureCoordinate;
 }
 );

NSString* GLVideoOutputColorMatrix709ProgramFragmentShader = SHADER_STRING
(
 uniform sampler2D SamplerY;
 uniform sampler2D SamplerUV;
 
 varying highp vec2 textureCoordinate;
 
 const mediump mat3 convertMatrix = mat3(
                                         1.164,  1.164, 1.164,
                                         0.0, -0.213, 2.112,
                                         1.793, -0.533,   0.0
                                         );
 
 void main()
 {
     mediump vec3 yuv;
     lowp vec3 rgb;
     
     // Subtract constants to map the video range start at 0
     yuv.x = texture2D(SamplerY, textureCoordinate).r - (16.0/255.0);
     yuv.yz = texture2D(SamplerUV, textureCoordinate).rg - vec2(0.5, 0.5);
     
     rgb = convertMatrix * yuv;
     
     gl_FragColor = vec4(rgb, 1.0);
 }
 );

@implementation GLVideoOutputColorMatrix709Program
{
    GLuint attribPosition, attribTexCoord;
    GLuint uniformSamplerY, uniformSamplerUV;
}

+ (instancetype)program:(GLContext *)context
{
    GLVideoOutputColorMatrix709Program* program = [[self alloc] initWithVertexShaderString:GLVideoOutputColorMatrix709ProgramVertexShader fragmentShaderString:GLVideoOutputColorMatrix709ProgramFragmentShader context:context];
    [program addAttribute:@"position"];
    [program addAttribute:@"inputTextureCoordinate"];
    if ([program link])
    {
        program->uniformSamplerY = [program uniformIndex:@"SamplerY"];
        program->uniformSamplerUV = [program uniformIndex:@"SamplerUV"];
        
        program->attribPosition = [program attributeIndex:@"position"];
        program->attribTexCoord = [program attributeIndex:@"inputTextureCoordinate"];
        
        glEnableVertexAttribArray(program->attribPosition);
        glEnableVertexAttribArray(program->attribTexCoord);
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Failed link program %@",NSStringFromClass([self class])];
    }
    
    return program;
}

#pragma mark -
#pragma mark GLVideoOutputProgramProtocol

- (GLuint)attribPosition
{
    return attribPosition;
}

- (GLuint)attribTexCoord
{
    return attribTexCoord;
}

- (GLuint)uniformSamplerY
{
    return uniformSamplerY;
}

- (GLuint)uniformSamplerUV
{
    return uniformSamplerUV;
}

@end
