//
//  GLVideoOutputColorMatrix709Program.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 6/14/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLProgram.h"
#import "GLVideoOutputProgramProtocol.h"

@interface GLVideoOutputColorMatrix709Program : GLProgram <GLVideoOutputProgramProtocol>

@end
