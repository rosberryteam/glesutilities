//
//  GLCameraOutputProgram.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 4/8/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLCameraOutputProgram.h"
#import "GLCommon.h"
#import "GLDebug.h"

NSString* const GLCameraOutputProgramVertexShader = SHADER_STRING
(
 attribute vec4 position;
 attribute vec2 inputTextureCoordinate;
 
 uniform float scale;
 
 varying vec2 textureCoordinate;
 
 void main()
 {
     gl_Position = vec4(position.xy * scale, 1.0, 1.0);
     textureCoordinate = inputTextureCoordinate;
 }
 );

NSString* const GLCameraOutputYUVFullRangeProgramFragmentShader = SHADER_STRING
(
 uniform sampler2D SamplerY;
 uniform sampler2D SamplerUV;
 
 varying highp vec2 textureCoordinate;
 
 const mediump mat3 convertMatrix = mat3(
                                         1.0,    1.0,        1.0,
                                         0.0,    -0.343,     1.765,
                                         1.4,    -0.711,     0.0
                                         );
 
 void main()
 {
     mediump vec3 yuv;
     
     yuv.x = texture2D(SamplerY, textureCoordinate).r;
     yuv.yz = texture2D(SamplerUV, textureCoordinate).ra - vec2(0.5);
     
     gl_FragColor = vec4(convertMatrix * yuv, 1.0);
 }
 );

NSString* const GLCameraOutputYUVVideoRangeProgramFragmentShader = SHADER_STRING
(
 uniform sampler2D SamplerY;
 uniform sampler2D SamplerUV;
 
 varying highp vec2 textureCoordinate;
 
 const mediump mat3 convertMatrix = mat3(
                                         1.164,  1.164,  1.164,
                                         0.0,    -0.392, 2.017,
                                         1.596,  -0.813, 0.0
                                         );
 
 void main()
 {
     mediump vec3 yuv;
     
     yuv.x = texture2D(SamplerY, textureCoordinate).r - 0.06274509803922; // 16.0 / 255.0
     yuv.yz = texture2D(SamplerUV, textureCoordinate).ra - vec2(0.5);
     
     gl_FragColor = vec4(convertMatrix * yuv, 1.0);
 }
 );

@implementation GLCameraOutputProgram

+ (instancetype)programWithSupportsFullYUVRange:(BOOL)programWithSupportsFullYUVRange context:(GLContext *)context
{
    NSString* fragShader = programWithSupportsFullYUVRange ? GLCameraOutputYUVFullRangeProgramFragmentShader : GLCameraOutputYUVVideoRangeProgramFragmentShader;
    GLCameraOutputProgram* program = [[self alloc] initWithVertexShaderString:GLCameraOutputProgramVertexShader fragmentShaderString:fragShader context:context];
    [program addAttribute:@"position"];
    [program addAttribute:@"inputTextureCoordinate"];
    if ([program link])
    {
        program->_uniformScale = [program uniformIndex:@"scale"];
        program->_uniformSamplerY = [program uniformIndex:@"SamplerY"];
        program->_uniformSamplerUV = [program uniformIndex:@"SamplerUV"];
        
        program->_attribPosition = [program attributeIndex:@"position"];
        program->_attribTexCoord = [program attributeIndex:@"inputTextureCoordinate"];
        
        glEnableVertexAttribArray(program.attribPosition);
        glEnableVertexAttribArray(program.attribTexCoord);
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Failed link program %@",NSStringFromClass([self class])];
    }
    
    return program;
}

@end
