//
//  GLCameraOutputProgram.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 4/8/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLProgram.h"

@interface GLCameraOutputProgram : GLProgram

@property (nonatomic,readonly) GLuint attribPosition, attribTexCoord;
@property (nonatomic,readonly) GLuint uniformScale, uniformSamplerY, uniformSamplerUV;

+ (instancetype)programWithSupportsFullYUVRange:(BOOL)programWithSupportsFullYUVRange context:(GLContext *)context;

@end
