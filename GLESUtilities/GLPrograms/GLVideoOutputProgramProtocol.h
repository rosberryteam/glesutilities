//
//  GLVideoOutputProgramProtocol.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 6/14/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GLVideoOutputProgramProtocol <NSObject>

- (GLuint)attribPosition;
- (GLuint)attribTexCoord;
- (GLuint)uniformSamplerY;
- (GLuint)uniformSamplerUV;

@end
