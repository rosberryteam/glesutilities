//
//  GLProgram.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 4/20/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLContext.h"
#import "GLCleanupProtocol.h"

#define STRINGIZE(x) #x
#define STRINGIZE2(x) STRINGIZE(x)
#define SHADER_STRING(text) @ STRINGIZE2(text)

#ifdef __cplusplus
class GLMatrix4x4;
#endif

@interface GLProgram : NSObject <GLContext, GLCleanupProtocol>

@property (nonatomic,readonly,getter=isInitialized) BOOL initialized;

+ (instancetype)program:(GLContext *)context;

- (instancetype)initWithVertexShaderString:(NSString *)vertex fragmentShaderString:(NSString *)fragment context:(GLContext *)context;
- (instancetype)initWithVertexShaderFileName:(NSString *)vertexFile fragmentShaderFileName:(NSString *)fragmentFile context:(GLContext *)context;

- (void)addAttribute:(NSString *)attributeName;
- (GLuint)attributeIndex:(NSString *)attributeName;
- (GLuint)uniformIndex:(NSString *)uniformName;

- (BOOL)link;
- (void)use;

- (void)validate;

- (void)setUniform1f:(GLint)location value:(GLfloat)value;
- (void)setUniform1i:(GLint)location value:(GLint)value;
- (void)setUniform2f:(GLint)location value:(CGPoint)value;

#ifdef __cplusplus
- (void)setUniformMatrix4x4:(GLint)location matrix:(const GLMatrix4x4 &)matrix;
#endif

@end
