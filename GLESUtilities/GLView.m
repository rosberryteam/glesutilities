//
//  GLView.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 10/14/13.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLView.h"
#import "GLDebug.h"
#import "GLLog.h"
#import "GLStateCache.h"

@interface GLView ()
{
    GLuint _colorRenderBuffer, _depthRenderBuffer, _displayFrameBuffer;
    GLuint _msaaColorRenderbuffer, _msaaFrameBuffer;
    
    GLsizei _numberMSAASamples;
    BOOL _isInitialize;
}

@end

@implementation GLView

+ (Class)layerClass
{
    return [CAEAGLLayer class];
}

#pragma mark -
#pragma mark Designated Initializer

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.contentScaleFactor = [GLContext scale];
    self.opaque = YES;
    
    CAEAGLLayer* layer = (CAEAGLLayer *)self.layer;
    layer.opaque = YES;
    layer.drawableProperties = @{kEAGLDrawablePropertyRetainedBacking:@(NO), kEAGLDrawablePropertyColorFormat:kEAGLColorFormatRGBA8};
    
    _options = GLViewOptionsNone;
    _numberMSAASamples = 2;
    _context = [GLContext sharedContext];
    _isInitialize = YES;
    
    [self.context lock:^() {
        
        [self destroyDisplayFramebuffer];
        [self createDisplayFramebuffer];
    }];
}

- (void)dealloc
{
    [self.context lock:^() {
        
        [self destroyDisplayFramebuffer];
    }];
}

#pragma mark -
#pragma mark Setters

- (void)setContext:(GLContext *)context
{
    if (_context != context)
    {
        [_context lock:^() {
            
            [self destroyDisplayFramebuffer];
        }];
        
        _context = context;
        
        [_context lock:^() {
            
            [self createDisplayFramebuffer];
        }];
    }
}

- (void)setFrame:(CGRect)frame
{
    CGSize size = self.bounds.size;
    
    [super setFrame:frame];
    
    if (!CGSizeEqualToSize(self.bounds.size, size))
    {
        [self recreateDisplayFramebuffer];
    }
}

- (void)setBounds:(CGRect)bounds
{
    BOOL recreateDisplayFramebuffer = !CGSizeEqualToSize(self.bounds.size, bounds.size);
    [super setBounds:bounds];
    
    if (recreateDisplayFramebuffer)
    {
        [self recreateDisplayFramebuffer];
    }
}

- (void)setOptions:(GLViewOptions)options
{
    if (_options != options)
    {
        _options = options;
        [self recreateDisplayFramebuffer];
    }
}

#pragma mark -
#pragma mark Create/Destroy main framebuffer

- (void)recreateDisplayFramebuffer
{
    if (!_isInitialize)
    {
        return;
    }
    
    [self.context lock:^() {
        
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache reset];
        }];
        
        [self destroyDisplayFramebuffer];
        [self createDisplayFramebuffer];
    }];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(glView:didRecreateDisplayFramebuffer:)])
    {
        [self.delegate glView:self didRecreateDisplayFramebuffer:self.sizeInPixels];
    }
}

- (void)createDisplayFramebuffer
{
    if (!_isInitialize || self.bounds.size.width == 0.0f || self.bounds.size.height == 0.0f)
    {
        return;
    }
    
    glGenFramebuffers(1, &_displayFrameBuffer);
    [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
        
        [cache bindFramebuffer:GL_FRAMEBUFFER framebuffer:_displayFrameBuffer];
    }];
    
    glGenRenderbuffers(1, &_colorRenderBuffer);
    [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
        
        [cache bindRenderbuffer:GL_RENDERBUFFER framebuffer:_colorRenderBuffer];
    }];
    
    if (![self.context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer])
    {
        GLLog(GLLogVerboseLevelError, @"Failed binds a drawable object’s storage to an OpenGL ES renderbuffer object.");
        [self destroyDisplayFramebuffer];
        return;
    }
    
    GLint backingWidth = 0, backingHeight = 0;
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
    
    if (backingWidth == 0 || backingHeight == 0)
    {
        GLLog(GLLogVerboseLevelError, @"Renderbuffer width/height cannot be 0");
        [self destroyDisplayFramebuffer];
        return;
    }
    
    _sizeInPixels.width = (CGFloat)backingWidth;
    _sizeInPixels.height = (CGFloat)backingHeight;
    
    GLLog(GLLogVerboseLevelWarning, @"Backing width: %d, height: %d", backingWidth, backingHeight);
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorRenderBuffer);
    
    if (self.options & GLViewOptionsMSAA)
    {
        GLint maxSamplesAllowed = 0;
        glGetIntegerv(GL_MAX_SAMPLES_APPLE, &maxSamplesAllowed);
        
        glGenFramebuffers(1, &_msaaFrameBuffer);
        NSAssert(_msaaFrameBuffer, @"Can't create default MSAA frame buffer");
        
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindFramebuffer:GL_FRAMEBUFFER framebuffer:_msaaFrameBuffer];
        }];
        
        glGenRenderbuffers(1, &_msaaColorRenderbuffer);
        NSAssert(_msaaColorRenderbuffer, @"Can't create MSAA color buffer");
        
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindRenderbuffer:GL_RENDERBUFFER framebuffer:_msaaColorRenderbuffer];
        }];
        
        glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, MIN(maxSamplesAllowed, _numberMSAASamples), GL_RGBA8_OES , backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _msaaColorRenderbuffer);
#if DEBUG
        GLuint framebufferStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        NSAssert(framebufferStatus == GL_FRAMEBUFFER_COMPLETE, @"Failure with MSAA framebuffer generation, error - 0x%X", framebufferStatus);
#endif
    }
    
    if (self.options & GLViewOptionsDepthBuffer)
    {
        glGenRenderbuffers(1, &_depthRenderBuffer);
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindRenderbuffer:GL_RENDERBUFFER framebuffer:_depthRenderBuffer];
        }];
        
        if (self.options & GLViewOptionsMSAA)
        {
            GLint maxSamplesAllowed = 0;
            glGetIntegerv(GL_MAX_SAMPLES_APPLE, &maxSamplesAllowed);
            glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, MIN(maxSamplesAllowed, _numberMSAASamples), GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        }
        else
        {
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        }
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _depthRenderBuffer);
        
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindRenderbuffer:GL_RENDERBUFFER framebuffer:_colorRenderBuffer];
        }];
    }
    
#if DEBUG
    GLuint framebufferStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    NSAssert(framebufferStatus == GL_FRAMEBUFFER_COMPLETE, @"Failure with display framebuffer generation for display of size: %zdx%zd", backingWidth, backingHeight);
#endif
}

- (void)destroyDisplayFramebuffer
{
    if (_displayFrameBuffer)
    {
        glDeleteFramebuffers(1, &_displayFrameBuffer);
        _displayFrameBuffer = 0;
    }
    
    if (_colorRenderBuffer)
    {
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindRenderbuffer:GL_RENDERBUFFER framebuffer:_colorRenderBuffer];
        }];
        [self.context renderbufferStorage:GL_RENDERBUFFER fromDrawable:nil];
        
        glDeleteRenderbuffers(1, &_colorRenderBuffer);
        _colorRenderBuffer = 0;
    }
    
    if (_depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &_depthRenderBuffer);
        _depthRenderBuffer = 0;
    }
    
    if (_msaaColorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &_msaaColorRenderbuffer);
        _msaaColorRenderbuffer = 0;
    }
    
    if (_msaaFrameBuffer)
    {
        glDeleteFramebuffers(1, &_msaaFrameBuffer);
        _msaaFrameBuffer = 0;
    }
    
    [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
        
        [cache reset];
    }];
}

#pragma mark -
#pragma mark Display

- (BOOL)display
{
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground)
    {
        [self.context lock];
        
        [self bindDrawable];
        [self.delegate glView:self drawInRect:self.bounds];
        [self presentFramebuffer];
        
        [self.context unlock];
        
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)bindDrawable
{
    if (self.options & GLViewOptionsMSAA)
    {
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindFramebuffer:GL_FRAMEBUFFER framebuffer:_msaaFrameBuffer];
        }];
    }
    else
    {
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindFramebuffer:GL_FRAMEBUFFER framebuffer:_displayFrameBuffer];
        }];
    }
}

- (void)presentFramebuffer
{
    if (self.options & GLViewOptionsMSAA)
    {
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindFramebuffer:GL_READ_FRAMEBUFFER_APPLE framebuffer:_msaaFrameBuffer];
            [cache bindFramebuffer:GL_DRAW_FRAMEBUFFER_APPLE framebuffer:_displayFrameBuffer];
        }];
        glResolveMultisampleFramebufferAPPLE();
    }
    
    if ([GLContext supportDiscardFramebuffer])
    {
        if (self.options & GLViewOptionsMSAA)
        {
            if (self.options & GLViewOptionsDepthBuffer)
            {
                GLenum attachments[] = {GL_COLOR_ATTACHMENT0, GL_DEPTH_ATTACHMENT};
                glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE, 2, attachments);
            }
            else
            {
                GLenum attachments[] = {GL_COLOR_ATTACHMENT0};
                glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE, 1, attachments);
            }
            
            [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
                
                [cache bindRenderbuffer:GL_RENDERBUFFER framebuffer:_colorRenderBuffer];
            }];
        }
        else if (self.options & GLViewOptionsDepthBuffer)
        {
            GLenum attachments[] = {GL_DEPTH_ATTACHMENT};
            glDiscardFramebufferEXT(GL_FRAMEBUFFER, 1, attachments);
        }
    }
    
    [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
        
        [cache bindRenderbuffer:GL_RENDERBUFFER framebuffer:_colorRenderBuffer];
    }];
    
    [self.context presentRenderbuffer:GL_RENDERBUFFER];
    
    if (self.options & GLViewOptionsMSAA)
    {
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindFramebuffer:GL_FRAMEBUFFER framebuffer:_msaaFrameBuffer];
        }];
    }
    
    glFlush();
}

#pragma mark -
#pragma mark Snapshots

static void GLViewDataProviderReleaseCallback(void *info, const void *data, size_t size)
{
    free((void *)data);
}

- (UIImage *)_snapshot
{
    CGSize size = self.sizeInPixels;
    NSUInteger dataSize = size.width * size.height * 4;
    GLubyte* data = malloc(dataSize);
    
    [self.delegate glView:self drawInRect:self.bounds];
    
    if (self.options & GLViewOptionsMSAA)
    {
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindFramebuffer:GL_READ_FRAMEBUFFER_APPLE framebuffer:_msaaFrameBuffer];
            [cache bindFramebuffer:GL_DRAW_FRAMEBUFFER_APPLE framebuffer:_displayFrameBuffer];
        }];
        glResolveMultisampleFramebufferAPPLE();
        
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindFramebuffer:GL_FRAMEBUFFER framebuffer:_displayFrameBuffer];
        }];
    }
    
    glPixelStorei(GL_PACK_ALIGNMENT, 4);
    glReadPixels(0, 0, size.width, size.height, GL_RGBA, GL_UNSIGNED_BYTE, data);
    
    CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, data, dataSize, GLViewDataProviderReleaseCallback);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGImageRef cgImage = CGImageCreate(size.width, size.height, 8, 32, 4 * size.width, colorSpace, kCGBitmapByteOrderDefault | kCGImageAlphaLast, dataProvider, NULL, NO, kCGRenderingIntentDefault);
    UIImage* image = [UIImage imageWithCGImage:cgImage scale:1.0f orientation:UIImageOrientationDownMirrored];
    
    CGDataProviderRelease(dataProvider);
    CGColorSpaceRelease(colorSpace);
    CGImageRelease(cgImage);
    
    return image;
}

- (UIImage *)snapshot
{
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground)
    {
        [self.context lock];
        UIImage* image = [self _snapshot];
        [self.context unlock];
        return image;
    }
    else
    {
        return nil;
    }
}

#pragma mark -
#pragma mark Debug

- (id)debugQuickLookObject
{
    return [self _snapshot];
}

@end
