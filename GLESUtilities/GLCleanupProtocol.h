//
//  GLCleanupProtocol.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 11/25/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GLCleanupProtocol <NSObject>
@required

- (void)cleanup;

@end
