//
//  GLVertexArray.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 5/8/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLContext.h"
#import "GLCleanupProtocol.h"

@interface GLVertexArray : NSObject <GLContext, GLCleanupProtocol>

@property (nonatomic,readonly) GLuint handle, vboHandle;
@property (nonatomic,readonly) NSUInteger dataSize;

+ (instancetype)vertexArrayWithDataSize:(NSUInteger)size context:(GLContext *)context;

- (void)bind;
- (void)unbind;

- (void)setupVertexAttrib:(GLuint)attrib size:(GLuint)size stride:(GLuint)stride offset:(GLuint)offset;
- (void)setupVertexAttrib:(GLuint)attrib size:(GLuint)size;

- (BOOL)map:(void(^)(void* data))block;

@end
