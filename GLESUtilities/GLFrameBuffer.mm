//
//  GLFrameBuffer.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 3/25/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLFrameBuffer.h"
#import "GLDebug.h"
#import "GLLog.h"
#import "GLProgram.h"
#import "GLStateCache.h"
#import "GLTexture.h"

NSString* NSStringFromGLFrameBufferFormat(GLFrameBufferFormat format)
{
    switch (format) {
        case GLFrameBufferFormatRGBA8888:   return @"RGBA8888";
        case GLFrameBufferFormatRGB565:     return @"RGB565";
        case GLFrameBufferFormatR8:         return @"R8";
        case GLFrameBufferFormatGLTexture:  return @"GLTexture";
        default:                            return @"Unknown";
    }
}

static void GLFrameBufferDataProviderReleaseCallback(void *info, const void *data, size_t size)
{
    free((void *)data);
}

@interface GLFrameBuffer ()

@property (nonatomic,getter=isInitialized) BOOL initialized;
@property (nonatomic) GLContext* context;

@end

@implementation GLFrameBuffer

+ (instancetype)frameBufferWithFormat:(GLFrameBufferFormat)format size:(CGSize)size context:(GLContext *)context
{
    return [[self alloc] initWithFormat:format size:size context:context];
}

+ (instancetype)frameBufferWithGLTexture:(GLTexture *)texture context:(GLContext *)context
{
    return [[self alloc] initWithGLTexture:texture context:context];
}

- (id)initWithFormat:(GLFrameBufferFormat)format size:(CGSize)size context:(GLContext *)context
{
    self = [super init];
    if (self)
    {
        NSAssert(size.width != 0.0f && size.height != 0.0f, @"Width and height cannot be 0!");
        NSAssert(format != GLFrameBufferFormatGLTexture, @"use -initWithGLTexture:context: instead!");
        NSParameterAssert(context);
        
        _context = context;
        _format = format;
        _size = [GLContext sizeThatFitInMaxSupportSize:size];
        
        if (self.format == GLFrameBufferFormatR8 && ![GLContext supportExtensionTextureRG])
        {
            GLLog(GLLogVerboseLevelWarning, @"WARNING: R8 pixel format unsupported, will be use RGB565");
            _format = GLFrameBufferFormatRGB565;
        }
        
        glGenFramebuffers(1, &_handle);
        glGenTextures(1, &_texHandle);
        
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindFramebuffer:GL_FRAMEBUFFER framebuffer:self.handle];
        }];
        
        GLuint format = 0;
        GLuint type = 0;
        
        switch (self.format)
        {
            case GLFrameBufferFormatRGBA8888:
                format = GL_RGBA;
                type = GL_UNSIGNED_BYTE;
                break;
            case GLFrameBufferFormatRGB565:
                format = GL_RGB;
                type = GL_UNSIGNED_SHORT_5_6_5;
                break;
            case GLFrameBufferFormatR8:
                format = GL_RED_EXT;
                type = GL_UNSIGNED_BYTE;
                break;
            default:
                break;
        }
        
        glBindTexture(GL_TEXTURE_2D, _texHandle);
        glTexImage2D(GL_TEXTURE_2D, 0, format, _size.width, _size.height, 0, format, type, NULL);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _texHandle, 0);
        
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE)
        {
            self.initialized = YES;
            GLLog(GLLogVerboseLevelWarning, @"GLFramebuffer::FBO Created - %@, size: %@", NSStringFromGLFrameBufferFormat(self.format), NSStringFromCGSize(self.size));
        }
        else
        {
            GLLog(GLLogVerboseLevelError, @"GLFramebuffer::Failed Create FBO: %@", self);
        }
        
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindFramebuffer:GL_FRAMEBUFFER framebuffer:0];
        }];
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    return self;
}

- (id)initWithGLTexture:(GLTexture *)texture context:(GLContext *)context
{
    self = [super init];
    if (self)
    {
        NSAssert(texture != nil, @"Pass nil");
        NSParameterAssert(context);
        
        _context = context;
        _format = GLFrameBufferFormatGLTexture;
        _size = texture.size;
        _texHandle = texture.texHandle;
        
        glGenFramebuffers(1, &_handle);
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindFramebuffer:GL_FRAMEBUFFER framebuffer:self.handle];
        }];
        
        glBindTexture(GL_TEXTURE_2D, texture.texHandle);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.texHandle, 0);
        
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE)
        {
            self.initialized = YES;
            GLLog(GLLogVerboseLevelWarning, @"GLFramebuffer::FBO Created with GLTexture - %@", texture);
        }
        else
        {
            GLLog(GLLogVerboseLevelError, @"GLFramebuffer::Failed Create FBO");
        }
        
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindFramebuffer:GL_FRAMEBUFFER framebuffer:0];
        }];
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    return self;
}

- (void)dealloc
{
    if (_handle || (_format != GLFrameBufferFormatGLTexture && _texHandle))
    {
        [self.context lock:^{ [self cleanup]; }];
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: %p> %@, handle: %zd, size: %@", [self class], self, NSStringFromGLFrameBufferFormat(self.format), self.handle, NSStringFromCGSize(self.size)];
}

- (void)bind
{
    if (self.isInitialized)
    {
        [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
            
            [cache bindFramebuffer:GL_FRAMEBUFFER framebuffer:self.handle];
        }];
    }
    else
    {
#if DEBUG
        [NSException raise:NSInternalInconsistencyException format:@"Perform -bind method without initialized framebuffer!"];
#endif
    }
}

- (void)unbind
{
    [GLStateCache beginWithContext:self.context block:^(id<GLStateCache> cache) {
        
        [cache bindFramebuffer:GL_FRAMEBUFFER framebuffer:0];
    }];
}

- (void)bindTextureToGLProgram:(GLProgram *)program uniform:(GLuint)uniform textureUnit:(GLenum)unit
{
    glActiveTexture(unit);
    glBindTexture(GL_TEXTURE_2D, self.texHandle);
    [program setUniform1i:uniform value:unit - GL_TEXTURE0];
}

#pragma mark -
#pragma mark Snapshots

- (UIImage *)snapshot
{
    if (!self.isInitialized)
    {
        return nil;
    }
    
    GLsizei width = (GLsizei)self.size.width;
    GLsizei height = (GLsizei)self.size.height;
    size_t dataSize = width * height * 4;
    GLubyte* pixels = (GLubyte *)calloc(1, dataSize);
    
    [self.context set];
    [self bind];
    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    [self unbind];
    
    CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, pixels, dataSize, GLFrameBufferDataProviderReleaseCallback);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGImageRef CGImage = CGImageCreate(width, height, 8, 32, 4 * width, colorSpace, kCGBitmapByteOrderDefault | kCGImageAlphaLast, dataProvider, NULL, NO, kCGRenderingIntentDefault);
    UIImage* image = [UIImage imageWithCGImage:CGImage];
    
    CGDataProviderRelease(dataProvider);
    CGColorSpaceRelease(colorSpace);
    CGImageRelease(CGImage);
    
    return image;
}

#pragma mark -
#pragma mark GLCleanupProtocol

- (void)cleanup
{
    if (_handle)
    {
        glDeleteFramebuffers(1, &_handle);
        _handle = 0;
    }
    
    if (_format != GLFrameBufferFormatGLTexture && _texHandle)
    {
        glDeleteTextures(1, &_texHandle);
        _texHandle = 0;
    }
}

#pragma mark -
#pragma mark Debug

- (id)debugQuickLookObject
{
    return [self snapshot];
}

@end
