//
//  GLTextureInfo.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 6/4/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import "GLCommon.h"
#import "GLTexture.h"

@interface GLTextureInfo : NSObject

@property (nonatomic,readonly) GLTextureFormat textureFormat;
@property (nonatomic,readonly) GLint format;
@property (nonatomic,readonly) GLint internalFormat;
@property (nonatomic,readonly) GLint type;
@property (nonatomic,readonly) GLsizei width;
@property (nonatomic,readonly) GLsizei height;
@property (nonatomic,readonly) CGSize size;

- (instancetype)initWithTextureFormat:(GLTextureFormat)textureFormat size:(CGSize)size;

@end
