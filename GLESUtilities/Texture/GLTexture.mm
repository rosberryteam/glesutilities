//
//  GLTexture.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 3/31/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLTexture.h"
#import "GLDebug.h"
#import "GLFrameBuffer.h"
#import "GLImageLoader.h"
#import "GLLog.h"
#import "GLProgram.h"
#import "GLTextureInfo.h"

#import <UIKit/UIKit.h>

@interface GLTexture ()

@property (nonatomic) GLContext* context;

@end

@implementation GLTexture

+ (instancetype)textureWithTextureFormat:(GLTextureFormat)textureFormat context:(GLContext *)context
{
    return [[self alloc] initWithTextureFormat:textureFormat context:context];
}

+ (instancetype)textureWithImage:(UIImage *)image andTextureFormat:(GLTextureFormat)textureFormat context:(GLContext *)context
{
    CGFloat width = CGImageGetWidth(image.CGImage);
    CGFloat height = CGImageGetHeight(image.CGImage);
    GLTexture* texture = [[self alloc] initWithTextureFormat:textureFormat context:context];
    [texture loadTextureWithImage:image size:CGSizeMake(width, height) error:nil];
    return texture;
}

+ (instancetype)emptyTextureWithSize:(CGSize)size andTextureFormat:(GLTextureFormat)textureFormat context:(GLContext *)context
{
    GLTexture* texture = [[self alloc] initWithTextureFormat:textureFormat context:context];
    texture->_size = size;
    
    glGenTextures(1, &texture->_texHandle);
    glBindTexture(GL_TEXTURE_2D, texture->_texHandle);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    GLTextureInfo* info = [[GLTextureInfo alloc] initWithTextureFormat:textureFormat size:size];
    glTexImage2D(GL_TEXTURE_2D, 0, info.format, info.width, info.height, 0, info.format, info.type, NULL);
    
    texture.isLoaded = YES;
    glBindTexture(GL_TEXTURE_2D, 0);
    
    return texture;
}

- (instancetype)initWithTextureFormat:(GLTextureFormat)format context:(GLContext *)context
{
    self = [super init];
    if (self)
    {
        NSParameterAssert(context);
        
        _context = context;
        _format = format;
    }
    return self;
}

- (void)dealloc
{
    GLLog(GLLogVerboseLevelWarning, @"%s", __func__);
    
    if (_texHandle)
    {
        [self.context lock:^{ [self cleanup]; }];
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@, %p>, isLoaded: %zd, %@, handle: %zd", [self class], self, self.isLoaded, NSStringFromCGSize(self.size), self.texHandle];
}

- (void)setIsLoaded:(BOOL)isLoaded
{
    [self willChangeValueForKey:@"isLoaded"];
    {
        _isLoaded = isLoaded;
    }
    [self didChangeValueForKey:@"isLoaded"];
}

#pragma mark -
#pragma mark Load / clear texture

- (BOOL)loadTextureWithImage:(UIImage *)image size:(CGSize)size error:(NSError **)error
{
    [self cleanup];
    
    GLTextureInfo* info = [[GLTextureInfo alloc] initWithTextureFormat:self.format size:size];
    GLImageLoader* imageLoader = [[GLImageLoader alloc] initWithTextureInfo:info];
    imageLoader.dataProvider = self.dataProvider;
    if ([imageLoader loadImage:image toTexture:self error:error])
    {
        _size = info.size;
        self.isLoaded = YES;
        
        return YES;
    }
    return NO;
}

- (BOOL)loadTextureWithData:(NSData *)data size:(CGSize)size error:(NSError **)error
{
    UIImage* image = [UIImage imageWithData:data];
    return [self loadTextureWithImage:image size:size error:error];
}

- (BOOL)loadTextureWithRAWData:(NSData *)data size:(CGSize)size error:(NSError **)error
{
    [self cleanup];
    
    if (data)
    {
        glGenTextures(1, &_texHandle);
        glBindTexture(GL_TEXTURE_2D, _texHandle);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        
        GLTextureInfo* info = [[GLTextureInfo alloc] initWithTextureFormat:self.format size:size];
        glTexImage2D(GL_TEXTURE_2D, 0, info.format, info.width, info.height, 0, info.format, info.type, data.bytes);
        
        glBindTexture(GL_TEXTURE_2D, 0);
        
        _size = info.size;
        self.isLoaded = YES;
        
        return YES;
    }
    else
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:@"GLTextureLoaderDomain" code:400 userInfo:@{NSLocalizedDescriptionKey:@"Data is nil"}];
        }
        
        return NO;
    }
}

- (void)bindTextureToGLProgram:(GLProgram *)program uniform:(GLuint)uniform textureUnit:(GLenum)unit
{
    glActiveTexture(unit);
    glBindTexture(GL_TEXTURE_2D, self.texHandle);
    [program setUniform1i:uniform value:unit - GL_TEXTURE0];
}

#pragma mark -
#pragma mark GLCleanupProtocol

- (void)cleanup
{
    self.isLoaded = NO;
    
    if (_texHandle)
    {
        glDeleteTextures(1, &_texHandle);
        _texHandle = 0;
    }
}

#pragma mark -
#pragma mark Debug

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
- (id)debugQuickLookObject
{
    GLFrameBuffer* fbo = [GLFrameBuffer frameBufferWithGLTexture:self context:self.context];
    return [fbo performSelector:@selector(debugQuickLookObject)];
}
#pragma clang diagnostic pop

@end
