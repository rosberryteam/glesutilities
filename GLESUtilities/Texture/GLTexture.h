//
//  GLTexture.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 3/31/14.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import "GLCleanupProtocol.h"
#import "GLContext.h"

@class GLContext;
@class GLProgram;
@class UIImage;

@protocol GLImageLoaderDataProvider;

typedef enum
{
    GLTextureFormatRGBA8888 = 0,
    GLTextureFormatRGBA5551,
    GLTextureFormatRGBA4444,
    GLTextureFormatRGB565
} GLTextureFormat;

@interface GLTexture : NSObject <GLContext, GLCleanupProtocol>

+ (instancetype)textureWithTextureFormat:(GLTextureFormat)textureFormat context:(GLContext *)context;
+ (instancetype)textureWithImage:(UIImage *)image andTextureFormat:(GLTextureFormat)textureFormat context:(GLContext *)context;
+ (instancetype)emptyTextureWithSize:(CGSize)size andTextureFormat:(GLTextureFormat)textureFormat context:(GLContext *)context;

@property (nonatomic,readonly) GLTextureFormat format;
@property (nonatomic,readonly) BOOL isLoaded;
@property (nonatomic,readonly) CGSize size;
@property (nonatomic,weak) id<GLImageLoaderDataProvider> dataProvider;
@property (nonatomic) GLuint texHandle;

- (BOOL)loadTextureWithImage:(UIImage *)image size:(CGSize)size error:(NSError **)error;
- (BOOL)loadTextureWithData:(NSData *)data size:(CGSize)size error:(NSError **)error;
- (BOOL)loadTextureWithRAWData:(NSData *)data size:(CGSize)size error:(NSError **)error;

- (void)bindTextureToGLProgram:(GLProgram *)program uniform:(GLuint)uniform textureUnit:(GLenum)unit;

@end
