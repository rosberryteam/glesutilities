//
//  GLImageLoader.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 6/5/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GLTexture;
@class GLTextureInfo;
@class UIImage;
@protocol GLImageLoaderDataProvider;

@interface GLImageLoader : NSObject

@property (nonatomic,weak) id<GLImageLoaderDataProvider> dataProvider;

- (instancetype)initWithTextureInfo:(GLTextureInfo *)info;

- (BOOL)loadImage:(UIImage *)image toTexture:(GLTexture *)texture error:(NSError **)error;

@end

@protocol GLImageLoaderDataProvider <NSObject>
@optional

- (void)imageLoader:(GLImageLoader *)imageLoader didLoadData:(NSData *)rawData;

@end
