//
//  GLTextureInfo.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 6/4/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLTextureInfo.h"
#import "GLContext.h"

@implementation GLTextureInfo

- (instancetype)initWithTextureFormat:(GLTextureFormat)textureFormat size:(CGSize)size
{
    self = [super init];
    if (self)
    {
        _textureFormat = textureFormat;
        
        size = [GLContext sizeThatFitInMaxSupportSize:size];
        
        _width = (GLsizei)size.width;
        _height = (GLsizei)size.height;
        _size = CGSizeMake(_width, _height);
        
        switch (textureFormat)
        {
            case GLTextureFormatRGBA8888:
            {
                _format = GL_RGBA;
                _internalFormat = GL_RGBA;
                _type = GL_UNSIGNED_BYTE;
                break;
            }
            case GLTextureFormatRGBA5551:
            {
                _format = GL_RGBA;
                _internalFormat = GL_RGBA;
                _type = GL_UNSIGNED_SHORT_5_5_5_1;
                break;
            }
            case GLTextureFormatRGBA4444:
            {
                _format = GL_RGBA;
                _internalFormat = GL_RGBA;
                _type = GL_UNSIGNED_SHORT_4_4_4_4;
                break;
            }
            case GLTextureFormatRGB565:
            {
                _format = GL_RGB;
                _internalFormat = GL_RGB;
                _type = GL_UNSIGNED_SHORT_5_6_5;
                break;
            }
            default:
                break;
        }
    }
    return self;
}

@end
