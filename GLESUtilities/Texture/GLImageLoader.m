//
//  GLImageLoader.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 6/5/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "GLImageLoader.h"
#import "GLContext.h"
#import "GLDebug.h"
#import "GLTextureInfo.h"

#import <UIKit/UIKit.h>

NSString* const GLImageLoaderDomain = @"GLImageLoaderDomain";

NS_INLINE CGImageAlphaInfo GLImageLoaderImageAlphaInfo(CGImageRef image)
{
    CGImageAlphaInfo alphaInfo = CGImageGetAlphaInfo(image);
    
    if (CGImageGetColorSpace(image))
    {
        if (alphaInfo == kCGImageAlphaPremultipliedLast || alphaInfo == kCGImageAlphaPremultipliedFirst || alphaInfo == kCGImageAlphaLast || alphaInfo == kCGImageAlphaFirst)
        {
            alphaInfo = kCGImageAlphaPremultipliedLast;
        }
        else
        {
            alphaInfo = kCGImageAlphaNoneSkipLast;
        }
    }
    return alphaInfo;
}

NS_INLINE CGAffineTransform GLImageLoaderImageTransform(UIImage* image, CGSize size)
{
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation)
    {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformRotate(CGAffineTransformTranslate(transform, size.width, size.height), M_PI);
            break;
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformRotate(CGAffineTransformTranslate(transform, size.width, 0.0f), M_PI_2);
            break;
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformRotate(CGAffineTransformTranslate(transform, 0.0f, size.height), -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (image.imageOrientation)
    {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformScale(CGAffineTransformTranslate(transform, size.width, 0.0f), -1.0f, 1.0f);
            break;
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformScale(CGAffineTransformTranslate(transform, size.height, 0.0f), -1.0f, 1.0f);
            break;
        default:
            break;
    }
    
    return transform;
}


@interface GLImageLoader ()

@property (nonatomic) GLTextureInfo* info;

@end

@implementation GLImageLoader

- (instancetype)initWithTextureInfo:(GLTextureInfo *)info
{
    self = [super init];
    if (self)
    {
        self.info = info;
    }
    return self;
}

- (BOOL)loadImage:(UIImage *)image toTexture:(GLTexture *)texture error:(NSError **)error
{
    if (image == nil)
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:GLImageLoaderDomain code:400 userInfo:@{NSLocalizedDescriptionKey:@"Image is nil"}];
        }
        return NO;
    }
    
    size_t dataSize = self.info.width * self.info.height * 4;
    void* data = calloc(1, dataSize);
    
    if (data == NULL)
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:GLImageLoaderDomain code:400 userInfo:@{NSLocalizedDescriptionKey:@"Failed allocate memory"}];
        }
        return NO;
    }
    
    CGImageRef CGImage = image.CGImage;
    CGImageAlphaInfo alphaInfo = GLImageLoaderImageAlphaInfo(CGImage);
    CGAffineTransform transform = GLImageLoaderImageTransform(image, self.info.size);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(data, self.info.width, self.info.height, 8, self.info.width * 4, colorSpace, alphaInfo | kCGBitmapByteOrder32Big);
    
    if (context == NULL)
    {
        free(data);
        CGColorSpaceRelease(colorSpace);
        
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:GLImageLoaderDomain code:400 userInfo:@{NSLocalizedDescriptionKey:@"Failed create CGContext"}];
        }
        return NO;
    }
    
    CGContextSetInterpolationQuality(context, kCGInterpolationMedium);
    CGContextConcatCTM(context, transform);
    
    switch (image.imageOrientation)
    {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, self.info.height, self.info.width), CGImage);
            break;
        default:
            CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, self.info.width, self.info.height), CGImage);
            break;
    }
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    GLuint texHandle = 0;
    
    glGenTextures(1, &texHandle);
    glBindTexture(GL_TEXTURE_2D, texHandle);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    BOOL result = YES;
    
    switch (texture.format)
    {
        case GLTextureFormatRGBA8888:
        {
            result = [self _load_RGBA8888_data_as_RGBA8888:data error:error];
            break;
        }
        case GLTextureFormatRGBA5551:
        {
            result = [self _load_RGBA8888_data_as_RGBA5551:data error:error];
            break;
        }
        case GLTextureFormatRGBA4444:
        {
            result = [self _load_RGBA8888_data_as_RGBA4444:data error:error];
            break;
        }
        case GLTextureFormatRGB565:
        {
            result = [self _load_RGBA8888_data_as_RGB565:data error:error];
            break;
        }
        default:
            break;
    }
    
    if (result)
    {
        texture.texHandle = texHandle;
    }
    else
    {
        glDeleteTextures(1, &texHandle);
    }
    
    glBindTexture(GL_TEXTURE_2D, 0);
    
    free(data);
    return result;
}

#pragma mark -
#pragma mark Private Conversion/Load Data Methods

- (BOOL)_load_RGBA8888_data_as_RGBA8888:(void *)data error:(NSError **)error
{
    glTexImage2D(GL_TEXTURE_2D, 0, self.info.format, self.info.width, self.info.height, 0, self.info.format, self.info.type, data);
    
    if ([self.dataProvider respondsToSelector:@selector(imageLoader:didLoadData:)])
    {
        size_t dataSize = self.info.width * self.info.height * 4;
        [self.dataProvider imageLoader:self didLoadData:[NSData dataWithBytesNoCopy:data length:dataSize freeWhenDone:NO]];
    }
    
    return YES;
}

- (BOOL)_load_RGBA8888_data_as_RGBA5551:(void *)data error:(NSError **)error
{
    void* tmp = malloc(self.info.width * self.info.height * 2);
    
    if (tmp == NULL)
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:GLImageLoaderDomain code:400 userInfo:@{NSLocalizedDescriptionKey:@"Failed allocate memory for texture conversion (RGBA8888 -> RGBA5551)"}];
        }
        return NO;
    }
    
    unsigned int* inPixel32 = (unsigned int*)data;
    unsigned short* outPixel16 = (unsigned short*)tmp;
    unsigned long size = self.info.width * self.info.height;
    
    for(unsigned int i = 0; i < size; ++i, ++inPixel32)
    {
        if ((*inPixel32 >> 31))                             // A can be 1 or 0
        {
            *outPixel16++ =
            ((((*inPixel32 >> 0) & 0xFF) >> 3) << 11) |     // R
            ((((*inPixel32 >> 8) & 0xFF) >> 3) << 6) |      // G
            ((((*inPixel32 >> 16) & 0xFF) >> 3) << 1) |     // B
            1; // A
        }
        else
        {
            *outPixel16++ = 0;
        }
    }
    
    glTexImage2D(GL_TEXTURE_2D, 0, self.info.format, self.info.width, self.info.height, 0, self.info.format, self.info.type, tmp);
    
    if ([self.dataProvider respondsToSelector:@selector(imageLoader:didLoadData:)])
    {
        size_t dataSize = self.info.width * self.info.height * 2;
        [self.dataProvider imageLoader:self didLoadData:[NSData dataWithBytesNoCopy:tmp length:dataSize freeWhenDone:NO]];
    }
    
    free(tmp);
    
    return YES;
}

- (BOOL)_load_RGBA8888_data_as_RGBA4444:(void *)data error:(NSError **)error
{
    void* tmp = malloc(self.info.width * self.info.height * 2);
    
    if (tmp == NULL)
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:GLImageLoaderDomain code:400 userInfo:@{NSLocalizedDescriptionKey:@"Failed allocate memory for texture conversion (RGBA8888 -> RGBA4444)"}];
        }
        return NO;
    }
    
    unsigned int* inPixel32 = (unsigned int*)data;
    unsigned short* outPixel16 = (unsigned short*)tmp;
    unsigned long size = self.info.width * self.info.height;
    
    for (unsigned int i = 0; i < size; ++i, ++inPixel32)
    {
        *outPixel16++ =
        ((((*inPixel32 >> 0) & 0xFF) >> 4) << 12) | // R
        ((((*inPixel32 >> 8) & 0xFF) >> 4) << 8) |  // G
        ((((*inPixel32 >> 16) & 0xFF) >> 4) << 4) | // B
        ((((*inPixel32 >> 24) & 0xFF) >> 4) << 0);  // A
    }
    
    glTexImage2D(GL_TEXTURE_2D, 0, self.info.format, self.info.width, self.info.height, 0, self.info.format, self.info.type, tmp);
    
    if ([self.dataProvider respondsToSelector:@selector(imageLoader:didLoadData:)])
    {
        size_t dataSize = self.info.width * self.info.height * 2;
        [self.dataProvider imageLoader:self didLoadData:[NSData dataWithBytesNoCopy:tmp length:dataSize freeWhenDone:NO]];
    }
    
    free(tmp);
    
    return YES;
}

- (BOOL)_load_RGBA8888_data_as_RGB565:(void *)data error:(NSError **)error
{
    void* tmp = malloc(self.info.width * self.info.height * 2);
    
    if (tmp == NULL)
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:GLImageLoaderDomain code:400 userInfo:@{NSLocalizedDescriptionKey:@"Failed allocate memory for texture conversion (RGBA8888 -> RGB565)"}];
        }
        return NO;
    }
    
    unsigned int* inPixel32 = (unsigned int*)data;
    unsigned short* outPixel16 = (unsigned short*)tmp;
    unsigned long size = self.info.width * self.info.height;
    
    // Convert RGBA8888 to RGB565
    for (unsigned long i = 0; i < size; ++i, ++inPixel32)
    {
        *outPixel16++ = ((((*inPixel32 >> 0) & 0xFF) >> 3) << 11) | ((((*inPixel32 >> 8) & 0xFF) >> 2) << 5) | ((((*inPixel32 >> 16) & 0xFF) >> 3) << 0);
    }
    
    glTexImage2D(GL_TEXTURE_2D, 0, self.info.format, self.info.width, self.info.height, 0, self.info.format, self.info.type, tmp);
    
    if ([self.dataProvider respondsToSelector:@selector(imageLoader:didLoadData:)])
    {
        size_t dataSize = self.info.width * self.info.height * 2;
        [self.dataProvider imageLoader:self didLoadData:[NSData dataWithBytesNoCopy:tmp length:dataSize freeWhenDone:NO]];
    }
    
    free(tmp);
    
    return YES;
}

@end
